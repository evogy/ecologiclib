
# EcologicLib

![EcologicLib logo](ecologiclib-logo.png)

**EcologicLib** is the SDK for Evogy's R&D projects and SimonLab platform.

# Docs
The following packages must be installed: `pip install -r docs/requirements.txt`.
Then build the docs: `sphinx-autobuild docs/source/ docs/build`