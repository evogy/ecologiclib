# API reference

## SimonLab I/O

### From SimonLab
```{eval-rst}
.. autofunction:: ecologiclib.get_simon_data
```
```{eval-rst}
.. autofunction:: ecologiclib.get_schedulation
```

### To SimonLab
```{eval-rst}
.. autofunction:: ecologiclib.write_df_on_simon_queue
```
```{eval-rst}
.. autofunction:: ecologiclib.write_value_on_simon_queue
```
```{eval-rst}
.. autofunction:: ecologiclib.send_command_to_simon
```

## SimonLab object CRUD

### Get functions
All *'get'* functions will return the requested SimonLab object (as python 
dictionary) if found, otherwise will return `None`.

#### Get by ID
```{eval-rst}
.. autofunction:: ecologiclib.get_plant_by_id
```
```{eval-rst}
.. autofunction:: ecologiclib.get_device_by_id
```
```{eval-rst}
.. autofunction:: ecologiclib.get_datapoint_by_id
```
```{eval-rst}
.. autofunction:: ecologiclib.get_driver_by_id
```
```{eval-rst}
.. autofunction:: ecologiclib.get_alarm_by_id
```
```{eval-rst}
.. autofunction:: ecologiclib.get_user_by_id
```
#### Get by code
```{eval-rst}
.. autofunction:: ecologiclib.get_plant_by_code
```
```{eval-rst}
.. autofunction:: ecologiclib.get_device_by_code
```
```{eval-rst}
.. autofunction:: ecologiclib.get_datapoint_by_code
```
#### Get by other parameter
```{eval-rst}
.. autofunction:: ecologiclib.get_driver_by_name
```
```{eval-rst}
.. autofunction:: ecologiclib.get_alarm_by_name
```
```{eval-rst}
.. autofunction:: ecologiclib.get_user_by_email
```

### Create functions
All *'create'* function return the created SimonLab object as python
dictionary. If you try to create a pre existing object, the old object
will be returned. *'create'* functions **don't update existing objects**.

```{eval-rst}
.. autofunction:: ecologiclib.create_device_if_not_exists
```
```{eval-rst}
.. autofunction:: ecologiclib.create_datapoint_if_not_exists
```
```{eval-rst}
.. autofunction:: ecologiclib.create_alarm_if_not_exists
```

### Update functions
All *'update'* functions return the updated SimonLab object (as python 
dictionary) if found. If you try to update a non existing object, the
function will return `None`. *'update'* functions **don't create new objects**.

```{eval-rst}
.. autofunction:: ecologiclib.update_device_if_exists
```
```{eval-rst}
.. autofunction:: ecologiclib.update_datapoint_if_exists
```


## Other services

```{eval-rst}
.. autofunction:: ecologiclib.get_weather_forecast
```