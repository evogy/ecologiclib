% EcologicLib documentation master file, created by
% sphinx-quickstart on Mon Feb 21 16:46:58 2022.
% You can adapt this file completely to your liking, but it should at least
% contain the root `toctree` directive.

```{include} ../../README.md 
:relative-images:
```

<!-- Read installation instructions in {ref}`Install EcologicLib` and how to use it in {doc}`api_ref`. -->

```{warning}
This library is under development!
```

```{toctree}
:caption: 'Contents:'
:maxdepth: 2

installation
api_ref
```