# Installation

## Install EcologicLib
To install EcologicLib, run:
```console
pip install git+https://eldritchhh@bitbucket.org/evogy/ecologiclib.git
```

## EcologicLib in 'requirements.txt'
EcologicLib can be added in a `requirements.txt` file using: 
```console
ecologiclib @ git+https://eldritchhh@bitbucket.org/evogy/ecologiclib.git
```