import logging
import requests
import datetime
import calendar
import pandas as pd
from urllib.parse import urljoin

logger = logging.getLogger(__name__)


def get_simon_data(
        url,
        token,
        plant,
        datapoints,
        date_to=None,
        date_from=None,
        lookback_hours=24,
        group_freq=None,
        raise_exception=True,
):
    header = {"Authorization": "Bearer " + token}

    if date_to is None:
        date_to = datetime.datetime.utcnow()

    if date_from is None:
        date_from = date_to - datetime.timedelta(hours=lookback_hours)

    date_to = calendar.timegm(date_to.utctimetuple())
    date_from = calendar.timegm(date_from.utctimetuple())

    data = []
    for dp in datapoints:
        params = f"/{plant}/{dp['device']}/{dp['datapoint']}/{date_from}/{date_to}"
        if group_freq is not None:
            params = params + f"/{group_freq}"

        if dp.get("grouping_func") is not None:
            params = params + f"/{dp['grouping_func']}"
            if dp.get("resampling_func") is not None:
                params = params + f"/{dp['resampling_func']}"

        req = requests.get(urljoin(url, params), headers=header)
        if req.status_code == 200:
            df = pd.read_json(req.json()["message"], orient="index")
            df.columns = [dp["alias"]]
            data.append(df)
        else:
            logger.error(
                f"Error occurred during get_simon_data API with error: \n {req.text}"
            )
            if raise_exception:
                raise requests.RequestException("Failed to retrieve Simon data")

    # TODO: questo serve perchè interpolando singolarmente ogni serie per dps con differnti frequenze di
    # camptionamento ottengo valori "null" perchè potrei non avere i valori di index.
    # ES: sono le 14:20, ho il dato della sonda meteo alle 14 -> avrò 3 "null"
    data = pd.concat(data, axis=1)
    return data.interpolate(method="linear", limit_direction="both")


def get_weather_forecast(
        url, token, lat, lon, n_days=2, group_freq="15T", raise_exception=True
):
    header = {"Authorization": "Bearer " + token}
    params = f"/weather/forecast/{lat}/{lon}/{n_days}/{group_freq}"
    req = requests.get(urljoin(url, params), headers=header)

    if req.status_code == 200:
        return pd.read_json(req.json()["message"], orient="index")
    else:
        logger.error(
            f"Error occurred during get_weather_forecast API with error: {req.text}"
        )
        if raise_exception:
            raise requests.RequestException("Failed to retrieve weather data")


def get_schedulation(
        url, token, dp_id, timeframe="daily", period="today", raise_exception=True
):
    header = {"Authorization": "Bearer " + token}
    params = f"/schedulation/{dp_id}/{timeframe}/{period}"
    req = requests.get(urljoin(url, params), headers=header)

    if req.status_code == 200:
        sched = req.json()["message"]
        if timeframe == "weekly":
            for d in list(calendar.day_name):
                if sched[d.lower()] is not None:
                    for s in sched[d.lower()]:
                        s["start"] = datetime.datetime.fromisoformat(s["start"])
                        s["end"] = datetime.datetime.fromisoformat(s["end"])
        else:
            if sched["today"] is not None:
                for s in sched["today"]:
                    s["start"] = datetime.datetime.fromisoformat(s["start"])
                    s["end"] = datetime.datetime.fromisoformat(s["end"])
        return sched
    else:
        logger.error(
            f"Error occurred during get_schedulation API with error: \n {req.text}"
        )
        if raise_exception:
            raise requests.RequestException("Failed to retrieve schedulation")


def check_if_point_exists(
        url, token, plant, device, datapoint, raise_exception=True
):
    header = {"Authorization": "Bearer " + token}
    params = f"/points?plantCode={plant}&deviceCode={device}&code={datapoint}"
    req = requests.get(urljoin(url, params), headers=header)

    if req.status_code == 200:
        point = req.json()
        if len(point) == 0:
            return False
        else:
            return True
    else:
        logger.error(
            f"Error occurred during check_if_datapoint_exists API with error: {req.text}"
        )
        if raise_exception:
            raise requests.RequestException("Failed to check if datapoint exists")
