from pymodm import fields, MongoModel, connect

import pytz
import json
import logging
import datetime
import pandas as pd

from ..simonlab_io import write_df_on_simon_queue, get_schedulation, get_simon_data
from ..simonlab_object_crud import create_point
from .utils import _compute_virtual_datapoints, _get_input_datapoints, _now_is_in_sched
from .plant import Plant, DataPoint, push_datapoint

logger = logging.getLogger(__name__)


class AnomalyRule(MongoModel):
    key = fields.CharField(primary_key=True)
    plant = fields.ReferenceField(Plant)
    alias = fields.CharField()
    assets = fields.ListField(fields.CharField())
    attributes = fields.ListField(
        fields.CharField(
            choices=["energy", "power", "co2", "humidity", "temperature", "custom"]
        )
    )
    method = fields.CharField(choices=["mixed_hour-day_zscore", "sched_range"])
    grouping_freq = fields.CharField()
    input_dp = fields.ReferenceField(DataPoint)
    output_dp = fields.ReferenceField(DataPoint)
    params = fields.DictField()
    last_datum = fields.DictField()

    def update_parameters(self, url, token):
        for asset_alias in self.assets:
            for attribute_alias in self.attributes:
                if attribute_alias == "custom":
                    data = self._get_custom_data(url, token)
                else:
                    data = self._get_data(url, token, asset_alias, attribute_alias)

                params = self._compute_rule_params(data.copy())
                if params is not None:
                    self.params[f"{asset_alias}_{attribute_alias}"] = params
        self.save()

    def apply(self, url, token, queue):
        for asset_alias in self.assets:
            for attribute_alias in self.attributes:
                if attribute_alias == "custom":
                    data = self._get_custom_data(url, token, 1).tail(1)
                else:
                    data = self._get_data(
                        url, token, asset_alias, attribute_alias, 1
                    ).tail(1)

                col = data.columns[-1]
                last_datum = pytz.utc.localize(
                    self.last_datum.get(
                        f"{asset_alias}_{attribute_alias}",
                        datetime.datetime(2020, 1, 1),
                    )
                )

                if data.index.item() > last_datum:
                    data = self._apply_rule(
                        data, col, asset_alias, attribute_alias, url, token
                    )
                    self.last_datum[f"{asset_alias}_{attribute_alias}"] = data.tail(
                        1
                    ).index.item()
                    self.save()

                    if attribute_alias == "custom":
                        device = self.output_dp.device
                        datapoint = self.output_dp.datapoint
                    else:
                        device = "anomaly_detection"
                        datapoint = f"{asset_alias}_{attribute_alias}_{self.method}"

                    write_df_on_simon_queue(
                        queue=queue,
                        plant=self.plant.id,
                        device=device,
                        datapoint=datapoint,
                        df=data,
                        col=col + "_anomaly_score",
                    )
                    logger.info(f"{self.alias} - Rule applied.")
                    # FIXME this return exits the "for loop"
                    return data
                else:
                    logger.info(f"{self.alias} - No new data available.")
                    return None

    def create_output_datapoints(self, url, token, device):
        for asset_alias in self.assets:
            for attribute_alias in self.attributes:
                if attribute_alias == "custom":
                    return
                dp_info = {
                    "code": f"{asset_alias}_{attribute_alias}_{self.method}",
                    "name": f"{asset_alias} - {attribute_alias} anomaly score",  # ({self.method})",
                    "unit": " ",
                    "tagId": "5c90c6185b30a5e204259fb8",
                    "valueType": "numeric",
                    "valuePrecision": 2,
                    "enumDictionary": [],
                }
                _ = create_datapoint(url, token, dp_info, device)

    def _get_custom_data(self, url, token, days=None):
        if days == None:
            days = self.params.get("lookback_window_days", 45)

        data = get_simon_data(
            url=url,
            token=token,
            plant=self.plant.id,
            datapoints=[self.input_dp.to_dict()],
            lookback_hours=int(days * 24),
            group_freq=self.grouping_freq,
        )
        return data

    def _get_data(self, url, token, asset_alias, attribute_alias, days=None):
        dps, real_dps = _get_input_datapoints(self.plant, asset_alias, attribute_alias)

        if days == None:
            days = self.params.get("lookback_window_days", 45)

        data = get_simon_data(
            url=url,
            token=token,
            plant=self.plant.id,
            datapoints=real_dps,
            lookback_hours=int(days * 24),
            group_freq=self.grouping_freq,
        )
        return _compute_virtual_datapoints(data, dps)


class SchedRangeAnomalyRule(AnomalyRule):
    def _compute_rule_params(self, data):
        return None

    def _apply_rule(self, data, col, asset_alias, attribute_alias, url, token):

        sched_id = self.plant.get_asset_by_alias(asset_alias).schedule_id
        sched = get_schedulation(url=url, token=token, dp_id=sched_id)  # DAILY - TODAY
        data = _now_is_in_sched(data, sched)

        min = (
            self.params.get("off_sched_min")
            if data["sched"].item() == 0
            else self.params.get("in_sched_min")
        )
        max = (
            self.params.get("off_sched_max")
            if data["sched"].item() == 0
            else self.params.get("in_sched_max")
        )

        if (data[col].item() > min) & (data[col].item() < max):
            data[f"{col}_anomaly_score"] = 0.0
        else:
            data[f"{col}_anomaly_score"] = 1.0
        return data


class MixedHDZScoreAnomalyRule(AnomalyRule):
    def _compute_rule_params(self, data):
        data["weekday"] = data.index.map(lambda d: d.weekday)
        data["hour"] = data.index.map(lambda d: d.hour)

        g_wd = data.groupby("weekday").agg(["mean", "std"])
        g_h = data.groupby(["weekday", "hour"]).agg(["mean", "std"])

        g_wd.columns = ["_".join(col) for col in g_wd.columns]
        g_h.columns = ["_".join(col) for col in g_h.columns]

        return {
            "g_wd": json.loads(g_wd.reset_index().to_json(orient="columns")),
            "g_h": json.loads(g_h.reset_index().to_json(orient="columns")),
        }

    def _apply_rule(self, data, col, asset_alias, attribute_alias, url, token):
        params = self.params[f"{asset_alias}_{attribute_alias}"]

        df = data.copy()
        df["weekday"] = df.index.map(lambda d: d.weekday)
        df["hour"] = df.index.map(lambda d: d.hour)

        g_wd = pd.DataFrame.from_dict(params.get("g_wd", None))
        g_h = pd.DataFrame.from_dict(params.get("g_h", None))

        df = df.reset_index().merge(g_wd, how="inner", on="weekday").set_index("index")
        df = (
            df.reset_index()
            .merge(g_h, how="inner", on=["weekday", "hour"], suffixes=["", "_h"])
            .set_index("index")
        )
        df = df.sort_index()

        data[f"{col}_anomaly_score"] = (df[col] - df[f"{col}_mean_h"]).abs() / df[
            f"{col}_std"
        ]
        return data


class AnomalyRuleHandler:
    def specialize(self, rule):
        cls = self._get_target_class(rule)
        return self._specialize_rule(cls, rule)

    def _get_target_class(self, rule):
        if rule.method == "sched_range":
            return SchedRangeAnomalyRule
        elif rule.method == "mixed_hour-day_zscore":
            return MixedHDZScoreAnomalyRule
        else:
            raise ValueError(f'Unexpected rule type: found "{rule.method}"')

    def _specialize_rule(self, cls, rule):
        new_rule = cls()
        for key, value in rule.__dict__.items():
            new_rule.__dict__[key] = value
        return new_rule


def push_anomaly_rule(url, token, config):
    input = {
        "key": f'{config["plant"]}_{config["alias"].replace(" ", "_")}',
        "plant": config["plant"],
        "alias": config["alias"],
        "assets": config["assets"],
        "attributes": config["attributes"],
        "method": config["method"],
        "grouping_freq": config["grouping_freq"],
    }

    if "custom" in config["attributes"]:
        try:
            input_dp = push_datapoint(config["input_dp"], config["plant"])
            output_dp = push_datapoint(config["output_dp"], config["plant"])
            input["input_dp"] = input_dp
            input["output_dp"] = output_dp
        except Exception:
            raise ValueError(
                "Attribute 'custom' need 'input_dp' and 'output_dp' parameters."
            )

    if config.get("params") is not None:
        input["params"] = config.get("params")

    anomaly_rule = AnomalyRule(**input)

    if anomaly_rule.plant is None:
        logger.error(
            f'Non plant found for id \'{config["plant"]}\'. Please push plant config file first.'
        )
        return
    else:
        for asset in anomaly_rule.assets:
            if asset not in [
                plant_asset.alias for plant_asset in anomaly_rule.plant.assets
            ]:
                logger.error(f"'{asset}' not found in referenced plant assets.")
                return

    device = anomaly_rule.plant.create_anomaly_detection_device(url, token)

    anomaly_rule.create_output_datapoints(url, token, device)

    anomaly_rule.save()
    return anomaly_rule


def push_anomaly_detection_config_file(url, token, db, config):
    connect(db)
    rules = [push_anomaly_rule(url, token, rule) for rule in config["anomaly_rules"]]
    logger.info(f"{len(list(filter(None, rules)))} anomaly rules correctly saved.")
