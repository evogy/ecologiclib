from pymodm import fields, MongoModel, connect

from .plant import Plant

class ControlRule(MongoModel):
    key = fields.CharField(primary_key=True)
    plant = fields.ReferenceField(Plant)
    alias = fields.CharField()
    assets = fields.ListField(fields.CharField())
    task = fields.CharField(choices=["watchdog", "sleep", "get_data"])
    grouping_freq = fields.CharField()
    params = fields.DictField()


def push_control_rule(config):
    input = {
        "key": f'{config["plant"]}_{config["alias"].replace(" ", "_")}',
        "plant": config["plant"],
        "alias": config["alias"],
        "assets": config["assets"],
        "task": config["task"],
        "grouping_freq": config.get("grouping_freq", "15min"),
    }
    if config.get("params") is not None:
        input["params"] = config.get("params")
    control_rule = ControlRule(**input)

    # TODO: qui potrei fare i controlli come faccio in "push_anomaly_rule" (es. se il plant esiste...)
    control_rule.save()
    return control_rule


def push_hvac_control_config_file(url, token, db, config):
    connect(db)
    rules = [push_control_rule(rule) for rule in config["hvac_control"]]
    # logger.info(f"{len(list(filter(None, rules)))} HVAC control rules correctly saved.")
