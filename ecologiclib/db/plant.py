import logging
from pymodm import fields, MongoModel, connect

from ..simonlab_object_crud import create_device_if_not_exists

logger = logging.getLogger(__name__)


class DataPoint(MongoModel):
    plant = fields.CharField()
    device = fields.CharField()
    datapoint = fields.CharField()
    alias = fields.CharField()
    triplet = fields.CharField(primary_key=True)
    grouping_func = fields.CharField()
    resampling_func = fields.CharField()

    def __str__(self):
        return f"DataPoint: plant -> {self.plant}, device -> {self.device}, datapoint -> {self.datapoint}, alias -> {self.alias}"

    def to_dict(self):
        return {
            "alias": self.alias,
            "device": self.device,
            "datapoint": self.datapoint,
            "grouping_func": self.grouping_func,
            "resampling_func": self.resampling_func,
        }


class VirtualDatapoint(MongoModel):
    alias = fields.CharField()
    transform = fields.CharField(choices=["mean", "sum"], blank=True)
    datapoints = fields.ListField(fields.ReferenceField(DataPoint))

    def __str__(self):
        return f"VirtualDataPoint: alias -> {self.alias}, datapoints -> {[self.datapoints_to_list]}"

    def datapoints_to_list(self):
        return [dp.to_dict() for dp in self.datapoints]

    def datapoints_alias_to_list(self):
        return [dp.alias for dp in self.datapoints]


class Damper(MongoModel):
    alias = fields.CharField()
    command_id = fields.CharField(primary_key=True)

    def __str__(self):
        return f"Damper: alias -> {self.alias}"


class ThermalZone(MongoModel):
    alias = fields.CharField()
    command_id = fields.CharField(primary_key=True)
    temperature_dp = fields.EmbeddedDocumentField(VirtualDatapoint)

    # PER ORA NON LO USIAMO E LEGGIAMO IL DATO "EFFETTIVO" DAL CAMPO.
    # output_dp rappresenta il datapoint su cui scrivere quello che fa l'algoritmo
    # (aka per i thermal, il setpoint impostato)
    # output_dp = fields.ReferenceField(DataPoint)

    # PER ORA NON LO USIAMO, VEDIAMO SE SERVE RILEGGERE DAL CAMPO O NO.
    # state_dp rappresenta il valore "riletto" per output_dp
    # (aka per i thermal il setpoint effettivo)
    # state_dp = fields.EmbeddedDocumentField(VirtualDatapoint)

    def __str__(self):
        return f"Thermal: alias -> {self.alias}"


class Asset(MongoModel):
    alias = fields.CharField()
    command_id = fields.CharField(primary_key=True)
    schedule_id = fields.CharField()

    ventilation_dp = fields.ReferenceField(DataPoint)
    energy_dp = fields.ReferenceField(DataPoint)
    power_dp = fields.ReferenceField(DataPoint)
    co2_dp = fields.EmbeddedDocumentField(VirtualDatapoint)
    humidity_dp = fields.EmbeddedDocumentField(VirtualDatapoint)
    # state_dp = fields.EmbeddedDocumentField(VirtualDatapoint)
    # output_dp = fields.ReferenceField(DataPoint)

    dampers = fields.ListField(fields.ReferenceField(Damper))
    thermal_zones = fields.ListField(fields.ReferenceField(ThermalZone))

    def __str__(self):
        return f"Fan: alias -> {self.alias}"


class ComfortBand(MongoModel):
    attribute = fields.CharField()
    min = fields.FloatField()
    max = fields.FloatField()
    delta = fields.FloatField()


class Room(MongoModel):
    key = fields.CharField(primary_key=True)
    alias = fields.CharField()
    comfort_bands = fields.ListField(fields.EmbeddedDocumentField(ComfortBand))
    assets = fields.ListField(fields.ReferenceField(Asset))

    def get_comfort_band(self, attribute):
        return [band for band in self.comfort_bands if band.attribute == attribute][0]


class Plant(MongoModel):
    alias = fields.CharField()
    id = fields.CharField(primary_key=True)
    lat = fields.FloatField()
    lon = fields.FloatField()

    season_id = fields.CharField()
    watchdog_id = fields.CharField()
    outer_temp_dp = fields.ReferenceField(DataPoint)

    rooms = fields.ListField(fields.ReferenceField(Room))
    assets = fields.ListField(fields.ReferenceField(Asset))

    def __str__(self):
        return f"Plant: id -> {self.id}, alias -> {self.alias}"

    def get_asset_by_alias(self, alias):
        return [asset for asset in self.assets if asset.alias == alias][0]

    def get_room_by_alias(self, alias):
        return [room for room in self.rooms if room.alias == alias][0]

    def create_anomaly_detection_device(self, url, token):
        return create_device_if_not_exists(
            url,
            token,
            self.id,
            "anomaly_detection",
            "Anomaly Detection",
            device_desc=None,
        )


def push_datapoint(dp_config, plant_id):
    triplet = f'{plant_id}_{dp_config["device"]}_{dp_config["datapoint"]}'
    dp = DataPoint(
        plant=plant_id,
        device=dp_config["device"],
        datapoint=dp_config["datapoint"],
        alias=dp_config.get("alias", triplet),
        grouping_func=dp_config.get("grouping_func", "mean"),
        resampling_func=dp_config.get("resampling_func", "linear"),
        triplet=triplet,
    )
    dp.save()
    return dp


def push_damper(damper_config):
    damper = Damper(
        alias=damper_config["alias"],
        command_id=damper_config["command_id"],
    )
    damper.save()
    return damper


def push_thermal_zone(thermal_zone_config, datapoints):
    thermal_zone_input = {
        "alias": thermal_zone_config["alias"],
        "command_id": thermal_zone_config["command_id"],
        "temperature_dp": _get_virtual_datapoint(
            thermal_zone_config.get("temperature_dp"), datapoints
        ),
    }

    thermal = ThermalZone(**thermal_zone_input)
    thermal.save()
    return thermal


def push_asset(asset_config, datapoints):
    asset_input = {
        "alias": asset_config["alias"],
        "command_id": asset_config["command_id"],
        "schedule_id": asset_config["schedule_id"],
        "ventilation_dp": _get_datapoint_by_alias(
            datapoints, asset_config.get("ventilation_dp")
        ),
        "energy_dp": _get_datapoint_by_alias(datapoints, asset_config.get("energy_dp")),
        "power_dp": _get_datapoint_by_alias(datapoints, asset_config.get("power_dp")),
        "thermal_zones": [
            push_thermal_zone(tz, datapoints)
            for tz in asset_config.get("thermal_zones")
        ],
    }

    co2_dp = _get_virtual_datapoint(asset_config.get("co2_dp"), datapoints)
    if co2_dp is not None:
        asset_input["co2_dp"] = co2_dp

    humidity_dp = _get_virtual_datapoint(asset_config.get("humidity_dp"), datapoints)
    if humidity_dp is not None:
        asset_input["humidity_dp"] = humidity_dp

    if asset_config.get("dampers") is not None:
        asset_input["dampers"] = [push_damper(d) for d in asset_config.get("dampers")]

    asset = Asset(**asset_input)
    asset.save()
    return asset


def push_room(room_config, assets, plant_id):
    room_input = {
        "key": f'{plant_id}_{room_config["alias"].replace(" ", "_")}',
        "alias": room_config["alias"],
        "comfort_bands": [ComfortBand(**band) for band in room_config["comfort_bands"]],
        "assets": [
            _get_asset_by_alias(assets, alias) for alias in room_config.get("assets")
        ],
    }
    room = Room(**room_input)
    room.save()
    return room


def push_plant_config_file(db, config):
    connect(db)
    config = config["plant"]
    plant_input = {
        "id": config["id"],
        "lat": config["lat"],
        "lon": config["lon"],
        "alias": config["alias"],
        "season_id": config["season_id"],
        "watchdog_id": config["watchdog_id"],
    }

    if config.get("datapoints") is not None:
        datapoints = [push_datapoint(dp, config["id"]) for dp in config["datapoints"]]
        plant_input["outer_temp_dp"] = _get_datapoint_by_alias(
            datapoints, config["outer_temp_dp"]
        )
        logger.info(f"{len(datapoints)} datapoints correctly saved.")
    else:
        logger.warning("No datapoints found.")

    if config.get("assets") is not None:
        assets = [push_asset(fan, datapoints) for fan in config["assets"]]
        logger.info(f"{len(assets)} assets correctly saved.")
        plant_input["assets"] = assets
    else:
        logger.warning("No assets found.")

    if config.get("rooms") is not None:
        rooms = [push_room(room, assets, config["id"]) for room in config["rooms"]]
        logger.info(f"{len(rooms)} rooms correctly saved.")
        plant_input["rooms"] = rooms
    else:
        logger.warning("No rooms found.")

    plant = Plant(**plant_input)
    plant.save()
    return plant


# UTILS


def _get_datapoint_by_alias(datapoints, alias):
    if alias is None:
        return None
    return [dp for dp in datapoints if dp.alias == alias][0]


def _get_asset_by_alias(assets, alias):
    if alias is None:
        return None
    return [asset for asset in assets if asset.alias == alias][0]


def _get_virtual_datapoint(config, datapoints):
    if config is None:
        return None

    if config.get("preprocess") is not None:
        dps = [
            _get_datapoint_by_alias(datapoints, alias)
            for alias in config["preprocess"]["datapoints"]
        ]
        return VirtualDatapoint(
            alias=config["alias"],
            transform=config["preprocess"]["transform"],
            datapoints=dps,
        )
    else:
        return VirtualDatapoint(
            alias=config["alias"],
            transform=None,
            datapoints=[_get_datapoint_by_alias(datapoints, config["alias"])],
        )
