import pandas as pd
from pymodm import fields, MongoModel, connect

import json
import pytz
import logging
import warnings
import datetime

from ..data_apis_helpers import get_schedulation, get_simon_data
from .plant import Plant
from .utils import (
    _compute_virtual_datapoints,
    _get_input_datapoints,
    _add_schedulation_to_data,
)

logger = logging.getLogger(__name__)
warnings.simplefilter(action="ignore", category=FutureWarning)


class RoomInfo(MongoModel):
    alias = fields.CharField()
    aggregation = fields.CharField(choices=["none", "mean", "add_mean"])
    excluded_assets = fields.ListField(fields.CharField())


class ReportRule(MongoModel):
    key = fields.CharField(primary_key=True)
    plant = fields.ReferenceField(Plant)
    alias = fields.CharField()
    rooms = fields.ListField(fields.EmbeddedDocumentField(RoomInfo))
    use_thermal_zones = fields.BooleanField()
    time_filter = fields.CharField(choices=["in_sched", "off_sched", "both", "single"])
    attributes = fields.ListField(
        fields.CharField(
            choices=[
                "energy",
                "power",
                "co2",
                "humidity",
                "temperature",
            ]  # TODO: custom
        )
    )
    type = fields.CharField(choices=["daily_kpi", "weekly_kpi", "daily_profile"])
    precedent = fields.CharField(choices=["one_week", "one_month"])
    grouping_freq = fields.CharField()
    params = fields.DictField()

    def compute_parameters(self, url, token):
        for room_info in self.rooms:
            room = self.plant.get_room_by_alias(room_info.alias)
            for attribute in self.attributes:
                attribute_data = None
                precedent_attribute_data = None
                cols = []
                for asset in room.assets:
                    # Compute assets params
                    agg = self._get_agg_function(attribute)
                    data = self._get_data(url, token, asset, attribute)
                    if data is None:
                        continue

                    # TODO: per il momento non prendo davvero la sched di ogni giorno ma replico
                    # quella della settimana scelta al momento della query
                    sched = get_schedulation(
                        url=url,
                        token=token,
                        dp_id=asset.schedule_id,
                        timeframe="weekly",
                        period="previous",
                    )
                    data = _add_schedulation_to_data(data, sched)
                    if (attribute != "temperature") or (
                        self.use_thermal_zones == False
                    ):
                        col = data.columns[-1]
                        self.params[
                            f"{asset.alias}_{attribute}"
                        ] = self._compute_rule_params(data.copy(), col, agg)

                        # Compute precedent params
                        if self.precedent is not None:
                            days = 15 if self.precedent == "one_week" else 38
                            precedent_data = self._get_data(
                                url, token, asset, attribute, days=days
                            )

                            precedent_data = precedent_data[
                                precedent_data.index < data.head(1).index.item()
                            ]
                            precedent_data = _add_schedulation_to_data(
                                precedent_data, sched
                            )
                            self.params[
                                f"{asset.alias}_{attribute}_precedent"
                            ] = self._compute_rule_params(precedent_data, col, agg)

                            precedent_attribute_data = (
                                precedent_data
                                if precedent_attribute_data is None
                                else precedent_attribute_data
                            )
                            precedent_attribute_data[col] = precedent_data[col]

                        attribute_data = (
                            data if attribute_data is None else attribute_data
                        )
                        attribute_data[col] = data[col]
                        cols.append(col)
                    else:
                        for tz in asset.thermal_zones:
                            col = tz.temperature_dp.alias
                            self.params[
                                f"{asset.alias}_{attribute}_{tz.alias}"
                            ] = self._compute_rule_params(data.copy(), col, agg)

                            # Compute precedent params
                            if self.precedent is not None:
                                days = 15 if self.precedent == "one_week" else 38
                                precedent_data = self._get_data(
                                    url, token, asset, attribute, days=days
                                )

                                precedent_data = precedent_data[
                                    precedent_data.index < data.head(1).index.item()
                                ]
                                precedent_data = _add_schedulation_to_data(
                                    precedent_data, sched
                                )
                                self.params[
                                    f"{asset.alias}_{attribute}_{tz.alias}_precedent"
                                ] = self._compute_rule_params(precedent_data, col, agg)

                                precedent_attribute_data = (
                                    precedent_data
                                    if precedent_attribute_data is None
                                    else precedent_attribute_data
                                )
                                precedent_attribute_data[col] = precedent_data[col]

                            attribute_data = (
                                data if attribute_data is None else attribute_data
                            )
                            attribute_data[col] = data[col]
                            cols.append(col)

                    # Compute room aggregated params
                    if attribute_data is None:
                        continue

                    attribute_data[
                        col.replace(asset.alias, room_info.alias)
                    ] = attribute_data[cols].agg(func=agg, axis=1)
                    self.params[
                        f"{room.alias}_{attribute}"
                    ] = self._compute_rule_params(
                        attribute_data.copy(),
                        col.replace(asset.alias, room_info.alias),
                        agg,
                    )

                    # Compute room aggregated precedent params
                    if self.precedent is not None:
                        precedent_attribute_data[
                            col.replace(asset.alias, room_info.alias)
                        ] = precedent_attribute_data[cols].agg(func=agg, axis=1)
                        self.params[
                            f"{room.alias}_{attribute}_precedent"
                        ] = self._compute_rule_params(
                            precedent_attribute_data.copy(),
                            col.replace(asset.alias, room_info.alias),
                            agg,
                        )
        self.save()

    def _get_data(self, url, token, asset, attribute_alias, days=8):
        dps, real_dps = _get_input_datapoints(self.plant, asset.alias, attribute_alias)
        if dps == []:
            return None

        data = get_simon_data(
            url=url,
            token=token,
            plant=self.plant.id,
            datapoints=real_dps,
            lookback_hours=int(days * 24),
            group_freq=self.grouping_freq,
        )

        data.index = data.index.tz_convert(pytz.timezone("Europe/Rome"))

        data = _compute_virtual_datapoints(data, dps)
        data = data[
            data.head(1).index.date.item()
            + datetime.timedelta(days=1) : data.tail(1).index.date.item()
        ]
        return data[:-1]

    # TODO: CHECK THIS!
    def _get_agg_function(self, attribute_alias):
        if attribute_alias == "energy":
            return "sum"
        if self.type != "daily_profile":
            if self.params.get("peak") is not None:
                return self.params.get("peak")
        return "mean"


class DailyKpiReportRule(ReportRule):
    def _compute_rule_params(self, data, col, agg_func):
        data["weekday"] = data.index.day_name()
        in_data = data[data["sched"] == 1]
        off_data = data[data["sched"] == 0]

        if self.time_filter == "in_sched":
            kpi_dict = self._compute_kpi(in_data, col, agg_func)

        if self.time_filter == "off_sched":
            kpi_dict = self._compute_kpi(off_data, col, agg_func)

        if self.time_filter == "both":
            kpi_dict_in = self._compute_kpi(in_data, col, agg_func)
            kpi_dict_in["in_sched"] = kpi_dict_in.pop("both")
            kpi_dict = self._compute_kpi(off_data, col, agg_func)
            kpi_dict["off_sched"] = kpi_dict.pop("both")
            kpi_dict.update(kpi_dict_in)

        if self.time_filter == "single":
            kpi_dict = self._compute_kpi(data, col, agg_func)

        return kpi_dict

    def _compute_kpi(self, data, col, agg_func):
        if agg_func == "sum":
            kpi = data[[col, "weekday"]].groupby(["weekday"]).sum()
        elif agg_func == "max":
            kpi = data[[col, "weekday"]].groupby(["weekday"]).max()
        elif agg_func == "min":
            kpi = data[[col, "weekday"]].groupby(["weekday"]).min()
        else:
            kpi = data[[col, "weekday"]].groupby(["weekday"]).mean()

        param_dict = {}
        for index, row in kpi.iterrows():
            param_dict[index] = row.item()

        return {self.time_filter: param_dict}


class WeeklyKpiReportRule(ReportRule):
    def _compute_rule_params(self, data, col, agg_func):
        in_data = data[data["sched"] == 1]
        off_data = data[data["sched"] == 0]

        if self.time_filter == "in_sched":
            kpi_dict = self._compute_kpi(in_data, col, agg_func)

        if self.time_filter == "off_sched":
            kpi_dict = self._compute_kpi(off_data, col, agg_func)

        if self.time_filter == "both":
            kpi_dict_in = self._compute_kpi(in_data, col, agg_func)
            kpi_dict_in["in_sched"] = kpi_dict_in.pop("both")
            kpi_dict = self._compute_kpi(off_data, col, agg_func)
            kpi_dict["off_sched"] = kpi_dict.pop("both")
            kpi_dict.update(kpi_dict_in)

        if self.time_filter == "single":
            kpi_dict = self._compute_kpi(data, col, agg_func)

        return kpi_dict

    def _compute_kpi(self, data, col, agg_func):
        if agg_func == "sum":
            kpi = data[col].sum()
        elif agg_func == "max":
            kpi = data[col].max()
        elif agg_func == "min":
            kpi = data[col].min()
        else:
            kpi = data[col].mean()
        return {self.time_filter: kpi}


class DailyProfileReportRule(ReportRule):
    def _compute_rule_params(self, data, col, agg_func):
        data = data[[col]]
        profile_dict = {
            group[0].strftime("%A"): json.loads(group[1].to_json(orient="columns"))
            for group in data.groupby(data.index.date)
        }
        return profile_dict


class ReportRuleHandler:
    def specialize(self, rule):
        cls = self._get_target_class(rule)
        return self._specialize_rule(cls, rule)

    def _get_target_class(self, rule):
        if rule.type == "daily_kpi":
            return DailyKpiReportRule
        elif rule.type == "weekly_kpi":
            return WeeklyKpiReportRule
        elif rule.type == "daily_profile":
            return DailyProfileReportRule
        else:
            raise ValueError(f'Unexpected rule type: found "{rule.type}"')

    def _specialize_rule(self, cls, rule):
        new_rule = cls()
        for key, value in rule.__dict__.items():
            new_rule.__dict__[key] = value
        return new_rule


def push_report_rule(config):
    input = {
        "key": f'{config["plant"]}_{config["alias"].replace(" ", "_")}',
        "plant": config["plant"],
        "alias": config["alias"],
        "time_filter": config.get("time_filter", "single"),
        "use_thermal_zones": config.get("use_thermal_zones", False),
        "attributes": config["attributes"],
        "type": config["type"],
        "grouping_freq": config.get("grouping_freq", "1H"),
    }

    if config.get("rooms") is not None:
        rooms = [_get_report_room(room) for room in config["rooms"]]
        input["rooms"] = rooms

    if config.get("precedent") is not None:
        input["precedent"] = config.get("precedent")

    if config.get("params") is not None:
        input["params"] = config.get("params")

    report_rule = ReportRule(**input)

    if report_rule.plant is None:
        logger.error(
            f'Non plant found for id \'{config["plant"]}\'. Please push plant config file first.'
        )
        return
    else:
        for room in report_rule.rooms:
            if room.alias not in [
                plant_room.alias for plant_room in report_rule.plant.rooms
            ]:
                logger.error(f"'{room.alias}' not found in referenced plant rooms.")
                return

    report_rule.save()
    return report_rule


def push_report_config_file(url, token, db, config):
    connect(db)
    rules = [push_report_rule(rule) for rule in config["report_rules"]]
    logger.info(f"{len(list(filter(None, rules)))} report rules correctly saved.")


# UTILS


def _get_report_room(config):
    input = {"alias": config["alias"], "aggregation": config["aggregation"]}
    if config.get("excluded_assets") is not None:
        input["excluded_assets"] = config["excluded_assets"]

    return RoomInfo(**input)
