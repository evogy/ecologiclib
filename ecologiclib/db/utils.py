import pytz
import datetime
import calendar
import pandas as pd
from .plant import DataPoint, VirtualDatapoint

pd.options.mode.chained_assignment = None


def _get_input_datapoints(plant, asset_alias, attribute_alias):
    asset = plant.get_asset_by_alias(asset_alias)

    if attribute_alias == "temperature":
        dps = [
            getattr(thermal_zone, "temperature_dp")
            for thermal_zone in asset.thermal_zones
        ]
    else:
        dps = [getattr(asset, f"{attribute_alias}_dp")]

    real_dps = []
    dps = [dp for dp in dps if dp is not None]
    if len(dps) > 0:
        for dp in dps:
            real_dps += (
                [dp.to_dict()] if type(dp) == DataPoint else dp.datapoints_to_list()
            )

    return dps, real_dps


def _compute_virtual_datapoints(data, dps):
    for dp in dps:
        if type(dp) == VirtualDatapoint:
            if dp.transform is not None:
                if dp.transform == "mean":
                    data[dp.alias] = data[dp.datapoints_alias_to_list()].mean(axis=1)
    if len(dps) > 1:
        data["mean"] = data[data.columns[-len(dps) :]].mean(axis=1)
    return data


def _add_schedulation_to_data(data, sched):
    cols = data.columns.tolist()

    data["sched"] = 0
    data["weekday"] = data.index.day_name()

    for day in list(calendar.day_name):
        for s in sched.get(day.lower()):
            view = data[data["weekday"] == day]
            start = s["start"].astimezone(pytz.timezone("Europe/Rome")).time()
            end = (
                s["end"].astimezone(pytz.timezone("Europe/Rome"))
                - datetime.timedelta(minutes=1)
            ).time()
            view["sched"].loc[start:end] = 1
            data[data["weekday"] == day] = view

    return data[["sched"] + cols]


def _now_is_in_sched(data, sched):
    cols = data.columns.tolist()

    data["sched"] = 0

    if sched["today"] is not None:
        for sched in sched["today"]:
            if sched["start"] <= data.index.item() <= sched["end"]:
                data["sched"] = 1

    return data[["sched"] + cols]
