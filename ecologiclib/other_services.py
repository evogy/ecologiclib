from datetime import timedelta

import pytz
import logging
import requests
import pandas as pd
from urllib.parse import urljoin

logger = logging.getLogger(__name__)


def get_weather_forecast(base_url, token, lat, lon, n_days=1, group_freq="1H"):
    r"""
    Return a pandas dataframe containing the weather forecast for the next ``n_days`` days
    (UTC) starting from today at 00:00 Europe/Rome. This imply the first N rows are past values
    based on what time the request is made.

    Args:
        base_url (str):
            base url for weatherapi api endopoint
        token (str):
            api key for weatherapi
        lat (str):
            latitude (degree)
        lon (str):
            longitude (degree)
        n_days (int):
            number of days starting from: today at 00:00 Europe/Rome
        group_freq (int):
            grouping frequency of the resulting pandas dataframe. default value is
            one hour because raw data are collected hourly

    Returns:
        dict: weather forecast as python dictionary. columns are
        ``outer_temperature``, ``cloud_cover``, ``rain_intensity`` and ``outer_humidity``

    Examples:
        >>> url = os.getenv("WEATHER_URL")
        >>> token = os.getenv("WEATHER_TOKEN")
        >>> forecast = get_weather_forecast(url, token, "9.8", "13.24", 3)
        >>> print(forecast.iloc[0])
        outer_temperature    29.1
        cloud_cover           0.0
        rain_intensity        0.0
        outer_humidity        9.0
        Name: 2022-03-03 23:00:00+00:00, dtype: float64
    """

    def _parse_data(forecast):
        data = []
        for day in forecast["forecast"]["forecastday"]:
            for h in day["hour"]:
                data.append(
                    {
                        "time": h["time"],
                        "outer_temperature": h["temp_c"],
                        "cloud_cover": h["cloud"],
                        "rain_intensity": h["precip_mm"],
                        "outer_humidity": h["humidity"],
                    }
                )

        location = forecast["location"]

        data = pd.DataFrame.from_records(data)
        data["time"] = (
            pd.to_datetime(data["time"])
            .dt.tz_localize(location["tz_id"])
            .dt.tz_convert(pytz.UTC)
        )
        return data.set_index("time")

    url = urljoin(base_url, f"?key={token}&q={lat},{lon}&days={n_days}")
    req = requests.get(url)

    if req.status_code == 200:
        data = _parse_data(req.json())

        if group_freq is not None:
            data = data.resample(group_freq).mean()
            data = data.interpolate("linear", limit_direcetion="both")
    else:
        logger.error(
            f"Error occurred during get_schedulation API with error: \n {req.text}"
        )
        return None

    return data


def get_weather_data(api_url, api_key, lat, lon, date_from, date_to):
    def fetch_data(start_date, end_date):
        url = (f"{api_url}/history/hourly?key={api_key}&lat={lat}&lon={lon}"
               f"&start_date={start_date}&end_date={end_date}&tz=utc")
        response = requests.get(url)
        if response.status_code == 200:
            return response.json()
        else:
            error_message = response.json().get('error', response.text)
            logger.error(f"Error occurred during get_weather_data API: {error_message}")
            raise requests.RequestException(f"Failed to retrieve weather data: {error_message}")

    date_from = pd.to_datetime(date_from)
    date_to = pd.to_datetime(date_to)

    all_data = []
    while date_from < date_to:
        chunk_end_date = min(date_from + timedelta(days=30), date_to)
        start_date_str = date_from.strftime("%Y-%m-%d")
        end_date_str = chunk_end_date.strftime("%Y-%m-%d")

        data_chunk = fetch_data(start_date_str, end_date_str)
        all_data.extend(data_chunk['data'])

        date_from = chunk_end_date #+ timedelta(days=1)

    df = pd.DataFrame(all_data)
    df.set_index('timestamp_utc', inplace=True)
    df.index = pd.to_datetime(df.index)
    df.index = df.index.tz_localize('UTC')
    return df