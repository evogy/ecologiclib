"""
Driver for RabbitMQ queue connection by AMQP protocol
"""
import pika
import logging

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class QueueManager:

    def __init__(self, config: dict, exchange: str):
        self._config = config
        self._exchange = exchange
        self._logger = logging.getLogger(__name__)
        self._create_connection()

    def _create_connection(self) -> None:
        """
        Create a connection to a queue
        """
        try:
            credentials = pika.PlainCredentials(self._config['USER'], self._config['PASSWORD'])
            parameters = pika.ConnectionParameters(host=self._config['HOST'], virtual_host=self._config['VIRTUAL_HOST'],
                                                   credentials=credentials)
            self._connection = pika.BlockingConnection(parameters)
            self._channel = self._connection.channel()
        except pika.exceptions.AMQPConnectionError as e:
            self._logger.error(e)
            raise

    @classmethod
    def simon_queue(cls, queue, exchange="amq.direct"):
        # return cls(QUEUES['simon-queue-queue'])
        return cls(queue, exchange)

    def close(self) -> None:
        """
        Close queue connection
        """
        self._connection.close()

    def publish(self, message: str, headers=None) -> None:
        """
        Publish a message on the queue
        """
        properties = None
        if headers is not None:
            properties = pika.BasicProperties(
                headers=headers,
                content_type='application/json',
            )

        self._channel.basic_publish(
            exchange=self._exchange,
            routing_key=self._config['QUEUE'],
            body=message,
            properties=properties,
        )

    def consume(self, callback) -> None:
        """
        Consume a message on the queue
        """
        self.message_received_callback = callback
        self._channel.basic_consume(
            self._config['QUEUE'],
            self._consume_message
        )
        self._channel.start_consuming()

    def _consume_message(self, channel: str, method: str, properties: str, body: str) -> None:
        """
        Callback to pass when a message is consumed
        """
        self.message_received_callback(body, properties)
        self._channel.basic_ack(delivery_tag=method.delivery_tag)

    def __repr__(self):
        return f'{self.__class__.__name__}({self._config!r})'
