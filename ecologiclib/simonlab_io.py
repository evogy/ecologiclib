import json
import pytz
import logging
import requests
import numpy as np
import pandas as pd
from urllib.parse import urljoin
from datetime import datetime, timedelta
from .db.plant import DataPoint
from .simonlab_object_crud import get_point_by_code

from .queuemanager import QueueManager

logger = logging.getLogger(__name__)



def get_simon_data(
    base_url,
    token,
    plant_code,
    datapoints,
    date_to=None,
    date_from=None,
    lookback_hours=24,
    group_freq="raw",
    get_raw_values=False,
    locale="Europe/Rome",
    series_group_func="none",
    kpi_group_func="none"
):
    if date_to is None:
        date_to = datetime.utcnow()

    if date_from is None:
        date_from = date_to - timedelta(hours=lookback_hours)

    date_from = pytz.timezone(locale).localize(date_from).isoformat()
    date_to = pytz.timezone(locale).localize(date_to).isoformat()

    formatted_datapoints = [
        {
            "plantCode": plant_code,
            "deviceCode": dp["device"],
            "code": dp["datapoint"],
            "extractionOperator": dp.get("grouping_func", "mean"),
        }
        for dp in datapoints
    ]

    input_data = {
        "tz": locale,
        "startDate": date_from,
        "endDate": date_to,
        "groupingTime": group_freq,
        "seriesGroupingFunc": series_group_func,
        "kpiGroupingFunc": kpi_group_func,
        "datapoints": formatted_datapoints,
    }

    header = {"Authorization": "Bearer " + token}
    url = urljoin(base_url, f"/charts/retrieve_silver_data")

    req = requests.post(url, headers=header, json=input_data)
    if req.status_code == 200:
        output_data = req.json()

        df = pd.DataFrame(data=output_data['data'], index=output_data['index'], columns=output_data['columns'])
        df.index = pd.to_datetime(df.index, unit='s')
        df.index = df.index.tz_localize("UTC").tz_convert(locale)
        return df
    else:
        print(
            f"Error occurred during 'non lo so' API with error: {req.text}"
        )
        return None


# NON SEMBRA FUNZIONARE
def get_last_value(base_url, token, plant_code, device_code, datapoint_code):
    header = {"Authorization": "Bearer " + token}
    url = urljoin(
        base_url,
        f"datapoint/plantCode/{plant_code}/deviceCode/{device_code}/datapointCode/{datapoint_code}/lastValue",
    )
    req = requests.get(url, headers=header)

    if req.status_code == 200:
        data = pd.DataFrame(req.json()["data"], columns=req.json()["columns"])
        if data.empty:
            return None
        data = data.set_index("time")
        data.index = pd.to_datetime(data.index)
        return data
    else:
        logger.error(
            f"Error occurred during get_last_value API from dp: '{plant_code} - {device_code} - {datapoint_code}' with error: {req.text}"
        )
        return None


def read_writable_datapoint(base_url, token, datapoint_id, formatted=False):
    header = {"Authorization": "Bearer " + token}
    url = urljoin(base_url, f"/points/{datapoint_id}")
    req = requests.get(url, headers=header)
    if req.status_code == 200:
        if formatted:
            return req.json()["lastValueFormatted"]
        else:
            return req.json()["lastValue"]


# TODO: a cosa serve ?
def check_if_datapoint_exists(base_url, token, plant_code, device_code, datapoint_code):
    header = {"Authorization": "Bearer " + token}
    url = urljoin(
        base_url,
        f"datapoint/plantCode/{plant_code}/deviceCode/{device_code}/datapointCode/{datapoint_code}/lastValueFL",
    )
    req = requests.get(url, headers=header)

    if req.status_code == 200:
        return True if req.json() != {} else False
    else:
        logger.error(
            f"Error occurred during check_if_datapoint_exists API from dp: '{plant_code} - {device_code} - {datapoint_code}' with error: {req.text}"
        )
        return None


def _get_sched(schedulations, utc_now):
    def parse_date(date_str: str):
        date = datetime.fromisoformat(date_str.replace("Z", ""))
        date_utc = pytz.timezone("UTC").localize(date)
        date_utc_as_eu_rome = date_utc.astimezone(pytz.timezone("Europe/Rome"))
        return date_utc_as_eu_rome

    def parse_profile_date(date_str: str, utc_now: datetime):
        date = datetime.fromisoformat(date_str.replace("Z", ""))
        date = date.replace(year=utc_now.year, month=utc_now.month, day=utc_now.day)
        date_eu_rome = pytz.timezone("Europe/Rome").localize(date)
        date_eu_rome_as_utc = date_eu_rome.astimezone(pytz.timezone("UTC"))
        return date_eu_rome_as_utc

    def get_schedule_time(entities: dict, utc_now: datetime):
        schedule_time = []
        weekday = utc_now.strftime("%A").lower()
        for sched in entities["weeklySetpoints"][weekday]:
            if sched["value"] != entities["defaultValue"]:
                start_utc = parse_profile_date(sched["start"], utc_now)
                end_utc = parse_profile_date(sched["end"], utc_now)
                if start_utc >= end_utc:
                    end_utc = end_utc + timedelta(days=1)
                schedule_time.append(
                    {
                        "start": start_utc,
                        "end": end_utc,
                        "value": sched["value"],
                    }
                )
        return {
            "default": entities["defaultValue"],
            "today": schedule_time if len(schedule_time) > 0 else None,
        }

    def date_is_in_range(date: datetime, start: str, end: str):
        start_utc = parse_date(start)
        end_utc = parse_date(end)
        return start_utc.date() <= date.date() <= end_utc.date()

    def date_is_in_wildcard(date: datetime, day: int, month: int):
        return (date.day == day) and (date.month == month)

    need_daily_sched = True
    daily_sched = None

    for schedulation in schedulations:
        if schedulation["schedulationType"] == "SpecialEvent":
            if schedulation["dateType"] == "BetweenTwoDates":
                if date_is_in_range(
                    utc_now, schedulation["start"], schedulation["end"]
                ):
                    need_daily_sched = False
            elif schedulation["dateType"] == "Wildcards":
                if date_is_in_wildcard(
                    utc_now, schedulation["anyDay"], schedulation["anyMonth"]
                ):
                    need_daily_sched = False

    for i, schedulation in enumerate(schedulations):
        if schedulation["schedulationType"] == "Profile":
            if schedulation["isDefault"] == False:
                if date_is_in_range(
                    utc_now, schedulation["start"], schedulation["end"]
                ):
                    if need_daily_sched:
                        daily_sched = get_schedule_time(
                            schedulation["linkedEntity"],
                            utc_now,
                        )
                    else:
                        daily_sched = {
                            "default": schedulation["linkedEntity"]["defaultValue"],
                            "today": None,
                        }
            else:
                default_profile_idx = i

    if daily_sched is None:
        if need_daily_sched:
            daily_sched = get_schedule_time(
                schedulations[default_profile_idx]["linkedEntity"],
                utc_now,
            )
        else:
            daily_sched = {
                "default": schedulations[default_profile_idx]["linkedEntity"][
                    "defaultValue"
                ],
                "today": None,
            }
    return daily_sched


def get_schedulation(
    base_url, token, datapoint_id, timeframe="daily", period="previous", offset=2
):
    r"""
    Return a python dictionary representing daily or weekly schedule.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        datapoint_id (str):
            mongodb id representing the datapoint
        timeframe (str):
            if ”daily” only today schedule is returned. if ”weekly” a daily
            schedule is returned for every weeekday.
        period (str):
            if ``timeframe`` is ”weekly” allow to choose ”next” or ”previous”
            week. ”previous” means the last seven days, ending today. ”next”
            means the next 7 days, today included.
        offset (int):
            offset hour. this offset is added to "now" in order to be sure you
            get the correct daily schedule if the request is made near midnight

    Returns:
        dict: daily or weekly schedule as python dictionary. key are the weekday
        for weekly schedule and "today" otherwise. in addition default profile value
        is returned (under the "default" key)

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> sched = get_plant_by_id(url, token, "620385e8834792001f9757e5")
        >>> print(sched)
        {
            'default': 24000,
            'today': [
                {'start': datetime.datetime(2022, 3, 4, 9, 30, tzinfo=<UTC>),
                'end': datetime.datetime(2022, 3, 4, 11, 0, tzinfo=<UTC>), 'value': 34000},
                {'start': datetime.datetime(2022, 3, 4, 17, 30, tzinfo=<UTC>),
                'end': datetime.datetime(2022, 3, 4, 19, 0, tzinfo=<UTC>), 'value': 34000}
            ]
        }
    """
    header = {"Authorization": "Bearer " + token}
    url = urljoin(base_url, f"writableDatapoints/{datapoint_id}/entitySchedulations/")
    req = requests.get(url, headers=header)

    if req.status_code == 200:
        if len(req.json()) == 0:
            return None

        now_utc = pytz.timezone("UTC").localize(datetime.utcnow()) + timedelta(
            hours=int(offset)
        )

        if timeframe == "daily":
            sched = _get_sched(req.json(), now_utc)
            return sched

        if timeframe == "weekly":
            scheds = {}
            for i in range(1, 8):
                if period == "previous":
                    weekday = now_utc - timedelta(days=i)
                elif period == "next":
                    weekday = now_utc + timedelta(days=i)
                daily_sched = _get_sched(req.json(), weekday)
                scheds[weekday.strftime("%A").lower()] = daily_sched["today"]
            scheds["default"] = daily_sched["default"]
            return scheds
    else:
        logger.error(
            f"Error occurred during get_schedulation API from dp: '{datapoint_id}' with error: \n {req.text}"
        )
        return None


# TO SIMON


def write_df_on_simon_queue(
    queue,
    plant_code,
    device_code,
    datapoint_code,
    df,
    col,
    raw_col=None,
    received_timestamp_col=None,
    source="other",
):
    r"""
    Write a pandas dataframe column on SimonLab RabbitMQ queue.

    Args:
        queue (dict):
            python dictionary representing the queue authentication parameters.
            it must contains the keys: ``USER``, ``QUEUE``, ``PASSWORD``,
            ``HOST`` and ``VIRTUAL_HOST``
        plant_code (str):
            simonlab code for plant
        device_code (str):
            simonlab code for device
        datapoint_code (str):
            simonlab code for datapoint
        df (pd.DataFrame):
            pandas dataframe with data
        col (str):
            the column in pandas dataframe `'df'` you want send to simonlab queue as `'value'`
        raw_col (str):
            the column in pandas dataframe `'df'` you want send to simonlab queue as `'raw value'`
        received_timestamp_col (str):
            the column in pandas dataframe `'df'` you want send to simonlab queue reprsenting `'raw value'` received timestamp

    Examples:
        >>> queue = {
        >>>    "USER": os.getenv("QUEUE_USER"),
        >>>    "QUEUE": os.getenv("QUEUE_NAME"),
        >>>    "PASSWORD": os.getenv("QUEUE_PASS"),
        >>>    "HOST": os.getenv("QUEUE_HOST"),
        >>>    "VIRTUAL_HOST": os.getenv("QUEUE_VIRTUAL_HOST"),
        >>> }
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> df = pd.DataFrame(np.random.randint(0,100,size=(100, 4)), columns=list('ABCD'))
        >>> write_df_on_simon_queue(queue, "0001", "StazioneMeteo", "temp", df, "B")
    """

    def _write_df_on_simon_queue(queue_obj, plant_code, device_code, datapoint_code, df, col):
        msg = []
        for idx, row in df.dropna().iterrows():
            values = {
                "installation": plant_code,
                "name": datapoint_code,
                "device": device_code,
                "timestamp": idx.isoformat(),
                "value": row[col].item(),
                "source": source,
            }
            if raw_col is not None:
                values["rawValue"] = row[raw_col].item()

            if received_timestamp_col is not None:
                values["receivedTimestamp"] = int(row[received_timestamp_col].item())
            msg.append(values)

        queue_obj.publish(json.dumps(msg))

    P = (len(df) // 99) + 1 if len(df) > 99 else 1
    partitions = np.array_split(df, P)

    queue_obj = QueueManager.simon_queue(queue)

    for partition in partitions:
        _write_df_on_simon_queue(
            queue_obj, plant_code, device_code, datapoint_code, partition, [col]
        )


def write_df_on_simon_queue_generated_data(
    queue,
    plant_code,
    device_code,
    datapoint_code,
    df,
    col,
    raw_col=None,
    received_timestamp_col=None,
    source="other",
    unit=""
):

    def _write_df_on_simon_queue_generated_data(queue_obj, plant_code, device_code, datapoint_code, df, col, unit, source):
        msg = []
        for idx, row in df.dropna().iterrows():
            values = {
                "plantCode": plant_code,
                "deviceCode": device_code,
                "code": datapoint_code,
                "timestamp": idx.isoformat(),
                # "receivedTimestamp": row["receivedTimestamp"].timestamp(),
                "measurementUnit": unit,
                "source": row.get("source", source),
                "value": row[col].item(),
            }

            if row.get("anomalyFlag") is not None:
                values["anomalyFlag"] = row.get("anomalyFlag")

            msg.append(values)

        queue_obj.publish(json.dumps(msg))

    P = (len(df) // 99) + 1 if len(df) > 99 else 1
    partitions = np.array_split(df, P)

    queue_obj = QueueManager.simon_queue(queue, "")

    for partition in partitions:
        _write_df_on_simon_queue_generated_data(
            queue_obj, plant_code, device_code, datapoint_code, partition, [col], unit, source
        )


def write_value_on_simon_queue(
    queue,
    plant_code,
    device_code,
    datapoint_code,
    timestamp,
    value,
    raw_value=None,
    received_timestamp=None,
    source="other",
):
    r"""
    Write a single value on SimonLab RabbitMQ queue.

    Args:
        queue (dict):
            python dictionary representing the queue authentication parameters.
            it must contains the keys: ``USER``, ``QUEUE``, ``PASSWORD``,
            ``HOST`` and ``VIRTUAL_HOST``
        plant_code (str):
            simonlab code for plant
        device_code (str):
            simonlab code for device
        datapoint_code (str):
            simonlab code for datapoint
        timestamp (str):
            string representing timestamp of value in format
            `ISO_8601 <https://it.wikipedia.org/wiki/ISO_8601>`_
        value (str):
            the `'value'` you want send to simonlab queue
        raw_col (str):
            the `'raw value'` you want send to simonlab queue
        received_timestamp_col (str):
            received timestamp for the `'raw value'`

    Examples:
        >>> queue = {
        >>>    "USER": os.getenv("QUEUE_USER"),
        >>>    "QUEUE": os.getenv("QUEUE_NAME"),
        >>>    "PASSWORD": os.getenv("QUEUE_PASS"),
        >>>    "HOST": os.getenv("QUEUE_HOST"),
        >>>    "VIRTUAL_HOST": os.getenv("QUEUE_VIRTUAL_HOST"),
        >>> }
        >>> timestamp = datetime.datetime.now().isoformat()
        >>> value = 12.5
        >>> write_value_on_simon_queue(queue, "0001", "StazioneMeteo", "temp", timestamp, value)
    """
    msg = []
    values = {
        "installation": plant_code,
        "name": datapoint_code,
        "device": device_code,
        "timestamp": timestamp,
        "value": value,
        "source": source,
    }
    if raw_value is not None:
        values["rawValue"] = raw_value

    if received_timestamp is not None:
        values["receivedTimestamp"] = received_timestamp

    msg.append(values)

    QueueManager.simon_queue(queue).publish(json.dumps(msg))


def write_value_on_simon_queue_generated_data(
    queue,
    plant_code,
    device_code,
    datapoint_code,
    timestamp,
    value,
    raw_value=None,
    received_timestamp=None,
    source="other",
    unit="",
    anomalyFlag=None
):
    msg = []
    values = {
        "plantCode": plant_code,
        "deviceCode": device_code,
        "code": datapoint_code,
        "timestamp": timestamp,
        "measurementUnit": unit,
        "source":  source,
        "value": value,
    }
    if anomalyFlag is not None:
        values["anomalyFlag"] = anomalyFlag

    msg.append(values)

    QueueManager.simon_queue(queue, "").publish(json.dumps(msg))


# TOdo ci pensiamo poi
def send_command_to_simon(
    url, token, action, duration=3660, enable_log=True, return_req=False
):
    r"""
    Send a command to a writable datapoint on SimonLab.

    Args:
        base_url (str):
            full url containing base simonlab url (prod or dev) and the
        token (str):
            bearer token for simonlab (prod or dev)
        action (int or str):
            the value you want to send to the writable datapoint specified in the url
        duration (int):
            number of command's validity seconds

    Examples:
        >>> base_url = os.getenv("SIMON_URL")
        >>> url = urljoin(base_url, "/")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> send_command_to_simon(url, token, 22)
    """
    body = {"value": {action}, "validityTime": duration}
    req = requests.post(url, data=body, headers={"Authorization": f"Bearer {token}"})
    if req.status_code != 200 and enable_log:
        logger.error(f"Something goes wrong sending a new command to Simon: {req.text}")
    if return_req:
        return req


def get_dp_from_tag(collection, plant_code, device_code, tag, more_info={}):
    # Cerca il dp a DB, se lo trova lo ritorna altrimenti ritorna None
    try:
        # region Cerca il dp a DB con il tag indicato e possibili ulteriori parametri
        found_dp = list(
            collection.find(
                {
                    "plantCode": plant_code,
                    "deviceCode": device_code,
                    "customTags.tagId": tag,
                    "deletedParents.0": {"$exists": False},
                    **more_info,
                }
            )
        )
        # endregion
        # region Datapoint non trovato, return None
        if len(found_dp) == 0:
            return None
        # endregion
        # region Più di un datapoint trovato, return il primo
        if len(found_dp) > 1:
            return {
                "device_code": found_dp[0]["deviceCode"],
                "dp_code": found_dp[0]["code"],
                "_id": str(found_dp[0]["_id"]),
                "fullPathName": found_dp[0]["name"],
            }
        # endregion
        # region Un unico datapoint trovato, return quello
        if len(found_dp) == 1:
            return {
                "device_code": found_dp[0]["deviceCode"],
                "dp_code": found_dp[0]["code"],
                "_id": str(found_dp[0]["_id"]),
                "fullPathName": found_dp[0]["name"],
            }
        # endregion
    except Exception as err:
        return None


def dp_device_from_tag(collection, plant_code, datapoint_tag, device_tag=None):
    r"""
    Return a datapoint specification from a tag.

    Args:
        collection (str): *
            collection mongo where find the datapoint
        plant_code (str): *
            simonlab code for plant
        device_tag (str):
            device tag to search for, default None
        datapoint_tag (str): *
             datapoint tag to search for

    Returns:
        dict: return datapoint object
    """
    try:
        # TODO: aggiungere questo campo quando dome lo implementa "deletedParents.0" : { $exists : false } per verificare  che il datapoint e il device esistano ancora e non siano stati soft eliminati
        datapoints = []
        if device_tag is None:
            datapoints = list(
                collection.find(
                    {"plantCode": plant_code, "customTags.tagId": datapoint_tag}
                )
            )
        else:
            datapoints = list(
                collection.find(
                    {
                        "plantCode": plant_code,
                        "deviceCustomTags.tagId": device_tag,
                        "customTags.tagId": datapoint_tag,
                    }
                )
            )
        if len(datapoints) < 0:
            Exception(
                f"Datapoint not found -> 'Plant Code': {plant_code} - 'Device Tag': {device_tag} 'Datapoint Tag': {datapoint_tag}"
            )
        return datapoints
    except Exception as e:
        print(e)
        return []
