import requests
from urllib.parse import urljoin


# GET ELEMENT BY (MONGODB) ID
def get_space_by_name(base_url, token, plant_code, space_name, parent_type=None):
    header = {"Authorization": "Bearer " + token}
    query = ""
    if parent_type is not None:
        query = f"&parentType={parent_type}"
    req = requests.get(urljoin(base_url, f"/spaces?name={space_name}&plantCode={plant_code}{query}"), headers=header)
    spaces = req.json() if req.status_code == 200 else None
    return spaces


def get_unique_space(base_url, token, code, parentId, parentType):
    header = {"Authorization": "Bearer " + token}
    query = f"code={code}&parentId={parentId}"
    req = requests.get(urljoin(base_url, f"/spaces?{query}"), headers=header)
    if req.status_code == 200:
        for elem in req.json():
            if elem["parentId"] == parentId:
                return elem
    else:
        return None


def get_space_by_id(base_url, token, space_id):
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/spaces/{space_id}"), headers=header)
    spaces = req.json() if req.status_code == 200 else None
    return spaces


def get_project_by_id(base_url, token, project_id):
    r"""
    Return a project as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        project_id (str):
            mongodb id representing the project

    Returns:
        dict: project as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> project = get_project_by_id(url, token, "5c48a4d516731f001444df68")
        >>> print(project["name"])
        Aris
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/projects/{project_id}"), headers=header)
    project = req.json() if req.status_code == 200 else None
    return project


def get_plant_by_id(base_url, token, plant_id):
    r"""
    Return a plant as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        plant_id (str):
            mongodb id representing the plant

    Returns:
        dict: plant as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> plant = get_plant_by_id(url, token, "5b31f65a202a5000145ab604")
        >>> print(plant["name"])
        The Bridge
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/plants/{plant_id}"), headers=header)
    plant = req.json() if req.status_code == 200 else None
    return plant


def get_device_by_id(base_url, token, device_id):
    r"""
    Return a device as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        device_id (str):
            mongodb id representing the device

    Returns:
        dict: device as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> device = get_device_by_id(url, token, "5d4d9e1259c98f00151eb261")
        >>> print(device["name"])
        Stazione Meteo
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/devices/{device_id}"), headers=header)
    device = req.json() if req.status_code == 200 else None
    return device


def get_device_by_plant_code(base_url, token, plant_code):
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/devices?plantCode={plant_code}"), headers=header)
    device = req.json() if req.status_code == 200 else None
    return device


def get_readable_datapoint_by_id(base_url, token, datapoint_id):
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/datapoints/{datapoint_id}"), headers=header)
    datapoint = req.json() if req.status_code == 200 else None
    return datapoint


def get_writable_datapoint_by_id(base_url, token, datapoint_id):
    header = {"Authorization": "Bearer " + token}
    req = requests.get(
        urljoin(base_url, f"/writableDatapoints/{datapoint_id}"), headers=header
    )
    datapoint = req.json() if req.status_code == 200 else None
    return datapoint


def get_point_by_id(base_url, token, datapoint_id):
    r"""
    Return a datapoint as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        datapoint_id (str):
            mongodb id representing the datapoint
        is_writable (bool):
            `True` if the datapoint is writable, `False` otherwise

    Returns:
        dict: datapoint as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> datapoint = get_device_by_id(url, token, "5d4d9e1459c98f00151eb31e")
        >>> print(datapoint["name"])
        Temperatura
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/points/{datapoint_id}"), headers=header)
    datapoint = req.json() if req.status_code == 200 else None
    return datapoint


def get_driver_by_id(base_url, token, driver_id):
    r"""
    Return a driver as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        driver_id (str):
            mongodb id representing the driver

    Returns:
        dict: driver as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> driver = get_driver_by_id(url, token, "619e5f2ad48b99001e0d2fcd")
        >>> print(driver["name"])
        Niagara
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/drivers/{driver_id}"), headers=header)
    driver = req.json() if req.status_code == 200 else None
    return driver


def get_alarm_by_id(base_url, token, alarm_id):
    r"""
    Return an alarm rule as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        alarm_id (str):
            mongodb id representing the alarm rule

    Returns:
        dict: alarm as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> alarm = get_alarm_by_id(url, token, "6214bd7f0827770017764938")
        >>> print(alarm["name"])
        Temperatura Esterna Elevata
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/alarmRules/{alarm_id}"), headers=header)
    alarm = req.json() if req.status_code == 200 else None
    return alarm


def get_user_by_id(base_url, token, user_id):
    r"""
    Return a user as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        user_id (str):
            mongodb id representing the user

    Returns:
        dict: user as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> user = get_user_by_id(url, token, "5faa59c3093f240017cf8447")
        >>> print(user["firstname"])
        Francesco
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/users/{user_id}"), headers=header)
    user = req.json() if req.status_code == 200 else None
    return user


def get_dashboard_by_id(base_url, token, dashboard_id):
    r"""
    Return a dashboard as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        dashboard_id (str):
            mongodb id representing the dashboard_id

    Returns:
        dict: user as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> dashboard = get_dashboard_by_id(url, token, "60ffc6cd795a97874b8117f7")
        >>> print(dashboard["name"])
        Dashboard Plant
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/dashboards/{dashboard_id}"), headers=header)
    dashboard = req.json() if req.status_code == 200 else None
    return dashboard


def get_chart_by_id(base_url, token, chart_id):
    r"""
    Return a chart as python dictionary using the MongoDb ID.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        chart_id (str):
            mongodb id representing the chart

    Returns:
        dict: chart as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> chart = get_user_by_id(url, token, "5cd973992726480014488fee")
        >>> print(chart["name"])
        Potenza
    """
    header = {"Authorization": "Bearer " + token}
    req = requests.get(urljoin(base_url, f"/charts/{chart_id}"), headers=header)
    user = req.json() if req.status_code == 200 else None
    return user


# Veniva utilizzato solo nella creazione delle dashboard (ad oggi non c'è il servizio che le crea)
# def get_chart_widget_by_id(base_url, token, chart_id):
#     r"""
#     Return a chart widget as python dictionary using id.
#
#     Args:
#         base_url (str):
#             base url for simonlab (prod or dev)
#         token (str):
#             bearer token for simonlab (prod or dev)
#         chart_id (str):
#             mongodb id representing the chart
#
#     Returns:
#         dict: chart widget as python dictionary
#
#     Examples:
#         >>> url = os.getenv("SIMON_URL")
#         >>> token = os.getenv("SIMON_TOKEN")
#         >>> widget = get_chart_widget_by_id(url, token, "63033b69727acc00152e186a")
#         >>> print(widget["name"])
#         Test Chart
#     """
#     header = {"Authorization": "Bearer " + token}
#     query = f"/{chart_id}/widget"
#     req = requests.post(urljoin(base_url, "/charts" + query), headers=header)
#     widget = req.json() if req.status_code == 200 else None
#     return widget


# GET ELEMENT BY CODE


def get_project_by_code(base_url, token, project_code):
    r"""
    Return a project as python dictionary using SimonLab code.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        project_code (str):
            simonlab code for project

    Returns:
        dict: project as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> project = get_plant_by_id(url, token, "Aris")
        >>> print(project["name"])
        Aris
    """
    header = {"Authorization": "Bearer " + token}
    query = f"?code={project_code}"
    req = requests.get(urljoin(base_url, "/projects" + query), headers=header)

    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def get_plant_by_code(base_url, token, plant_code):
    r"""
    Return a plant as python dictionary using SimonLab code.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        plant_code (str):
            simonlab code for plant

    Returns:
        dict: plant as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> plant = get_plant_by_id(url, token, "0001")
        >>> print(plant["name"])
        The Bridge
    """
    header = {"Authorization": "Bearer " + token}
    query = f"?code={plant_code}"
    req = requests.get(urljoin(base_url, "/plants" + query), headers=header)

    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def get_space_by_code(base_url, token, space_code, plantCode=None):
    header = {"Authorization": "Bearer " + token}
    query = f"?code={space_code}"
    if plantCode is not None:
        query += f"&plantCode={plantCode}"
    req = requests.get(urljoin(base_url, "/space" + query), headers=header)

    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def get_space(base_url, token, space_code, plant_code=None, space_name=None):
    header = {"Authorization": "Bearer " + token}
    query = f"?code={space_code}"
    if plant_code is not None:
        query += f"&plantCode={plant_code}"
    if space_name is not None:
        query += f"&name={space_name}"
    req = requests.get(urljoin(base_url, "/space" + query), headers=header)

    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def get_device_by_code(base_url, token, plant_code, device_code):
    r"""
    Return a device as python dictionary using SimonLab code.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        plant_code (str):
            simonlab code for plant
        device_code (str):
            simonlab code for device

    Returns:
        dict: device as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> device = get_device_by_code(url, token, "0001", "StazioneMeteo")
        >>> print(device["name"])
        Stazione Meteo
    """
    header = {"Authorization": "Bearer " + token}
    query = f"?plantCode={plant_code}&code={device_code}"
    req = requests.get(
        urljoin(base_url, "/devices" + query),
        headers=header,
    )

    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def _request_datapoint_by_code(url, header):
    req = requests.get(url, headers=header)
    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def get_point_by_code(base_url, token, plant_code, device_code, point_code):
    r"""
    Return a datapoint as python dictionary using SimonLab code.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        plant_code (str):
            simonlab code for plant
        device_code (str):
            simonlab code for device
        datapoint_code (str):
            simonlab code for datapoint
        is_writable (bool):
            `True` if the datapoint is writable, `False` otherwise

    Returns:
        dict: datapoint as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> device = get_device_by_code(url, token, "0001", "StazioneMeteo", "temp")
        >>> print(datapoint["name"])
        Temperatura
    """
    header = {"Authorization": "Bearer " + token}
    query = f"?plantCode={plant_code}&deviceCode={device_code}&code={point_code}"
    url = urljoin(base_url, "/points" + query)
    return _request_datapoint_by_code(url, header)


def get_driver_by_name(base_url, token, plant_code, driver_name, return_default=False):
    r"""
    Return a driver as python dictionary using SimonLab plant code and driver name.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        plant_code (str):
            simonlab code for plant
        driver_name (str):
            driver's name
        return_default (bool):
            `True` if you want the default driver as return when
            the correct driver cannot be found, `False` otherwise

    Returns:
        dict: driver as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> driver = get_driver_by_name(url, token, "0001", "Niagara")
        >>> print(driver["name"])
        Niagara
    """
    header = {"Authorization": "Bearer " + token}
    plant = get_plant_by_code(base_url, token, plant_code)
    query = f"?plantId={plant['id']}"
    req = requests.get(
        urljoin(base_url, "/drivers" + query),
        headers=header,
    )

    if req.status_code == 200:
        for driver in req.json():
            if driver["name"] == driver_name:
                return driver
    if return_default:
        return req.json()[0] if (len(req.json()) > 0) else None
    return None


def get_alarm_by_name(
        base_url, token, plant_code, device_code, datapoint_code, alarm_name, single=True
):
    r"""
    Return an alarm as python dictionary using SimonLab datapoint code and alarm name.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        plant_code (str):
            simonlab code for plant
        device_code (str):
            simonlab code for device
        datapoint_code (str):
            simonlab code for datapoint
        alarm_name (str):
            alarm rule's name

    Returns:
        dict: alarm as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> alarm = get_alarm_by_name(
        >>>     url, token, "0001", "StazioneMeteo", "temp", "Temperatura Esterna Elevata"
        >>> )
        >>> print(alarm["name"])
        Temperatura Esterna Elevata
    """
    header = {"Authorization": "Bearer " + token}
    datapoint = get_point_by_code(
        base_url, token, plant_code, device_code, datapoint_code
    )
    query = f"?datapointId={datapoint['readableId']}&name={alarm_name}"
    req = requests.get(
        urljoin(base_url, "/alarmRules" + query),
        headers=header,
    )
    if req.status_code == 200:
        if single:
            return [req.json()[0]] if len(req.json()) > 1 else req.json()
        else:
            return req.json()
    # if req.status_code == 200:
    #     for driver in req.json():
    #         if driver["name"] == alarm_name:
    #             return driver
    # return None
    return None


def get_user_by_email(base_url, token, email):
    r"""
    Return a user as python dictionary using email.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        email (str):
            user's email

    Returns:
        dict: user as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> user = get_user_by_email(url, token, "francesco.prete@evogy.it")
        >>> print(user["firstname"])
        Francesco
    """
    header = {"Authorization": "Bearer " + token}
    query = f"?email={email}"
    req = requests.get(urljoin(base_url, "/users" + query), headers=header)
    user = req.json()[0] if req.status_code == 200 else None
    return user


def get_dashboard_by_name(
        base_url, token, parent_code, dashboard_name, parent_type=None
):
    r"""
    Return a dashboard as python dictionary using SimonLab plant code and dashboard name.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        plant_code (str):
            simonlab code for plant
        dashboard_name (str):
            dashboard's name
        parent_type (str):
            type of parent. Must be "`Plant`" or "`Project`".
            If not specified, parent type will be set to "`Plant`"
    Returns:
        dict: alarm as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> dashboard = get_dashboard_by_name(url, token, "0001", "Dashboard Plant")
        >>> print(dashboard["name"])
        Dashboard Plant
    """
    header = {"Authorization": "Bearer " + token}
    parent_type = "Plant" if parent_type is None else parent_type

    if parent_type == "Plant":
        parent = get_plant_by_code(base_url, token, parent_code)
    else:
        parent = get_project_by_code(base_url, token, parent_code)

    query = f"?parentId={parent['id']}&name={dashboard_name}"
    req = requests.get(
        urljoin(base_url, "/dashboards" + query),
        headers=header,
    )
    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def get_alarm_cause_by_name(base_url, token, name):
    header = {"Authorization": "Bearer " + token}
    url = f"{base_url}/labels?name={name}&category=alarmCauses"
    req = requests.get(url, headers=header)
    if req.status_code == 200:
        return req.json()[0]["id"]
    else:
        return None


# CREATE


def _prune(dict):
    to_drop = [
        key.replace("Ids", "").replace("Id", "")
        for key in dict.keys()
        if (key[-2:] == "Id") or (key[-3:] == "Ids")
    ]
    for key in to_drop:
        dict.pop(key, None)
    return dict


def create_plant_if_not_exists(base_url, token, plant_info):
    def _complete_plant_info(plant_info):
        if plant_info.get("area") is None:
            plant_info["area"] = "1"
        if plant_info.get("description") is None:
            plant_info["description"] = None
        if plant_info.get("tz") is None:
            plant_info["tz"] = "Rome"
        if plant_info.get("sector") is None:
            plant_info["sector"] = "Terziario/Istruzione, Sanità, Assistenza Sociale"
        if plant_info.get("service_type") is None:
            plant_info["service_type"] = "Monitoring"
        if plant_info.get("installation_date") is None:
            plant_info["installation_date"] = "2020-01-01"
        if plant_info.get("capex") is None:
            plant_info["capex"] = "1"
        if plant_info.get("opex") is None:
            plant_info["opex"] = "1"
        if plant_info.get("contract_years") is None:
            plant_info["contract_years"] = "1"
        if plant_info.get("targer_perc") is None:
            plant_info["targer_perc"] = 0.15
        return plant_info

    plant = get_plant_by_code(base_url, token, plant_info["code"])
    if plant is None:
        new_plant_info = _complete_plant_info(plant_info)
        plant = create_plant(base_url, token, new_plant_info)

    return plant


def create_plant(base_url, token, plant_info):
    header = {"Authorization": "Bearer " + token}
    template_data = {
        "isInMaintenance": False,
        "code": plant_info["code"],
        "name": plant_info["name"],
        "area": plant_info["area"],
        "description": plant_info["description"],
        "parentId": plant_info["parent_id"],
        "customTags": {"tz": plant_info["tz"], "sector": plant_info["sector"]},
        "mapWidgetDatapointsIds": [],
        "serviceType": plant_info["service_type"],
        "simonInstallationDate": plant_info["installation_date"],
        "filterableFieldCAPEX": plant_info["capex"],
        "filterableFieldOPEX": plant_info["opex"],
        "filterableFieldContractYears": plant_info["contract_years"],
        "filterableFieldSavingTargetPercentage": plant_info["targer_perc"],
    }

    req = requests.post(
        urljoin(base_url, "/plants"), json=template_data, headers=header
    )
    if req.status_code != 201:
        return None

    plant = req.json()
    widget_data = {
        "widgets": [
            {
                "coordinates": {"x": 0, "y": 0, "w": 3, "h": 1},
                "name": plant_info["name"],
                "description": "",
                "widgetType": "availability",
                "widgetInfo": {
                    "color": "#f0f3f5",
                    "plantId": plant["id"],
                },
                "temporaryUploadedImage": [],
                "temporaryUploadedImageUrl": None,
                "multiTimeFrameOptions": {},
            }
        ],
        "id": plant["id"],
    }
    req = requests.put(
        urljoin(base_url, f"/plants/{plant['id']}"), json=widget_data, headers=header
    )
    if req.status_code != 200:
        return None

    location_data = {
        "name": plant_info["name"],
        "address": plant_info["address"],
        "coordinates": [plant_info["lon"], plant_info["lat"]],
        "parentEntityId": plant["id"],
        "parentEntityType": "Plant",
    }
    req = requests.post(
        urljoin(base_url, "/locations"), json=location_data, headers=header
    )
    if req.status_code != 201:
        return None

    return plant


def create_device(base_url, token, device_info):
    header = {"Authorization": "Bearer " + token}
    template_data = {
        "code": device_info["device_code"],
        "name": device_info["device_name"],
        "description": device_info["device_description"],
        "iconId": device_info["device_icon_id"],
        "parentId": device_info["parent_id"],
        "datapointsIds": [],
        "customTags": {"tagId": device_info["device_tag"]},
        "writableDatapointsIds": [],
        "parentType": device_info["parentType"],
    }
    return requests.post(
        urljoin(base_url, "/devices"), json=template_data, headers=header
    ).json()


def create_space(base_url, token, space_info):
    header = {"Authorization": "Bearer " + token}
    template_data = {
        "type": space_info["space_type"],
        "code": space_info["space_code"],
        "name": space_info["space_name"],
        "parentId": space_info["parent_id"],
        "parentType": space_info["parent_type"],
        "plantCode": space_info["plant_code"]
    }
    return requests.post(
        urljoin(base_url, "/spaces"), json=template_data, headers=header
    ).json()


def get_list_of_subspaces(base_url, token, entity_id, entity_type="Plant"):
    header = {"Authorization": "Bearer " + token}
    query = f"?entityType={entity_type}&entityId={entity_id}"
    req = requests.get(urljoin(base_url, "/spaces/subSpaces" + query), headers=header)
    if req.status_code == 200:
        return req.json()
    return None


def create_space_if_not_exists(base_url, token, space_info):
    def _complete_space_info(base_url, token, space_info):
        if space_info.get("space_type") is None:
            space_info["space_type"] = "Building"
        if space_info.get("parent_type") is None:
            space_info["parentType"] = "Plant"
        if space_info.get("parent_id") is None:
            space_info["parent_id"] = get_plant_by_code(
                base_url, token, space_info["plant_code"]
            )["id"]
        return space_info

    space = get_space(
        base_url, token, space_info["space_code"], space_info["plant_code"], space_info["space_name"]
    )
    if space is None:
        new_space_info = _complete_space_info(base_url, token, space_info)
        space = create_space(base_url, token, new_space_info)

    return space


# chiedere per parentType
def create_device_if_not_exists(base_url, token, device_info):
    r"""
    Create a device using a parameter dictionary and return the
    new device as python dictionary.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        device_info (dict):
            parameter dictionary. It must contains the following parameters:

            - ``plant_code`` (str): simonlab code for plant
            - ``device_code`` (str): new device code
            - ``device_name`` (str): new device name
            - ``parentType`` (str): "Plant" or ??

            You can also specify the following optionale parameters:

            - ``device_description`` (str): new device description
            - ``device_icon_id`` (str): mongodb id representing the new device
              icon
            - ``device_tag_id`` (str): mongodb id representing the new
              device haystack tag. If not specified, the new device will be
              created as 'Sonda (generica)'

    Returns:
        dict: new device as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> device_info = {
        >>>     "plant_code" : "0001",
        >>>     "device_code" : "new_device",
        >>>     "device_name" : "New Device!"
        >>> }
        >>> device = create_device_if_not_exists(url, token, device_info)
        >>> print(device["name"])
        New Device!
    """

    def _complete_device_info(base_url, token, device_info):
        if device_info.get("device_description") is None:
            device_info["device_description"] = ""
        if device_info.get("device_icon_id") is None:
            device_info["device_icon_id"] = "5c90c885b68336001f67b052"
        if device_info.get("device_tag") is None:
            device_info["device_tag"] = "SONDA_GENERICA"
        if device_info.get("parent_id") is None:
            device_info["parent_id"] = get_plant_by_code(
                base_url, token, device_info["plant_code"]
            )["id"]
        if device_info.get("parentType") is None:
            device_info["parentType"] = "Plant"
        return device_info

    device = get_device_by_code(
        base_url, token, device_info["plant_code"], device_info["device_code"]
    )
    if device is None:
        new_device_info = _complete_device_info(base_url, token, device_info)
        device = create_device(base_url, token, new_device_info)

    return device


# refs
def create_device_link_refs(base_url, token, ref_info):
    header = {"Authorization": "Bearer " + token}
    template_data = {
        "deviceIdToLink": ref_info["deviceIdToLink"],
        "refId": ref_info["refId"],
    }
    return requests.post(
        urljoin(base_url, f"/devices/{ref_info['deviceIdFromLink']}/link"), json=template_data, headers=header
    ).json()


def get_refs_by_device_id(base_url, token, device_id):
    header = {"Authorization": "Bearer " + token}
    refs = requests.get(
        urljoin(base_url, f"/devices/{device_id}/links"), headers=header
    ).json()
    if len(refs) == 0:
        return None
    return refs


def create_refs_if_not_exists(base_url, token, ref_info):
    # {
    #     "deviceFromPlantCode": row.get("plantCode"),
    #     "deviceFromCode": row.get("deviceCode"),
    #     "type": row.get("typeRefs")[index],
    #     "deviceToPlantCode": row.get("devicePlantCodeToRef")[index],
    #     "deviceToCode": row.get("deviceCodeToRef")[index]
    # }
    device_from_ref = get_device_by_code(base_url, token, ref_info["deviceFromPlantCode"], ref_info["deviceFromCode"])
    device_to_ref = get_device_by_code(base_url, token, ref_info["deviceFromPlantCode"], ref_info["deviceCodeToRef"])

    # se uno dei due device non esiste annulla
    if device_from_ref is None or device_to_ref is None:
        return None

    # ottieni i ref del device attuale
    actual_refs = get_refs_by_device_id(base_url, token, device_from_ref["_id"])

    if actual_refs is not None:
        for ref in actual_refs:
            if ref["device"]["_id"] == device_to_ref["_id"]:
                return ref

    ref = get_refs_by_name(base_url, token, ref_info["typeRefs"])
    if ref is None:
        return None
    new_ref = create_device_link_refs(base_url, token, {
        "deviceIdFromLink": device_from_ref["_id"],
        "deviceIdToLink": device_to_ref["_id"],
        "refId": ref.get("_id"),
    })
    if new_ref.get("statusCode") == 200:
        return new_ref
    else:
        return None


def get_refs_by_name(base_url, token, ref_name):
    header = {"Authorization": "Bearer " + token}
    query = f"?name={ref_name}"
    req = requests.get(
        urljoin(base_url, "/refs" + query),
        headers=header,
    )

    if req.status_code == 200:
        if len(req.json()) == 1:
            return req.json()[0]
    return None


def _inject_point_template(dp_info, driver):
    template = {
        "type": dp_info["dp_type"],
        "deviceCode": dp_info["device_code"],
        "code": dp_info["dp_code"],
        "customTags": {"tagId": dp_info["dp_tag"]},
        "deviceId": dp_info["device_id"],
        "name": dp_info["dp_name"],
        "description": dp_info["dp_description"],
        "infrastructuralTags": [],
        "measurementUnit": dp_info["dp_unit"],
        "valueType": dp_info["dp_value_type"],
        "operators": ["alarms", "influxTimeseries", "mongoLastReadings"],
        "extractionOperator": dp_info["dp_extraction_operator"],
        "valuePrecision": dp_info["dp_value_precision"],
        "enumDictionary": dp_info["dp_enum_dictionary"],
        "isVirtual": False if dp_info.get("virtual_parameters") is None else True,
        "virtualParameters": dp_info.get("virtual_parameters"),
        "plantCode": dp_info["plant_code"],
        "estimatedDataCadence": dp_info["dp_frequency"],
        "hisMode": dp_info["dp_his_mode"],
        "hisTotalized": dp_info["dp_his_totalized"],
        "isWritable": dp_info["dp_is_writable"],
        "isReadable": True,
        "originalDriverEntityId": dp_info["original_driver_entity_id"],
    }

    if driver is not None:
        template["originalDriverId"] = driver["id"]
        template["commandDriver"] = driver

    return template


def create_point(base_url, token, datapoint_info, driver=None):
    header = {"Authorization": "Bearer " + token}
    template_data = _inject_point_template(datapoint_info, driver)
    return requests.post(
        urljoin(base_url, "/points"), json=template_data, headers=header
    ).json()


def _inject_constant_point_template(dp_info):
    template = {
        "type": "constant",
        "isConstant": True,
        "isReadable": True,
        "deviceCode": dp_info["device_code"],
        "code": dp_info["dp_code"],
        "customTags": {"tagId": dp_info["dp_tag"]},
        "deviceId": dp_info["device_id"],
        "name": dp_info["dp_name"],
        "valuePrecision": dp_info["dp_value_precision"],
        "description": dp_info["dp_description"],
        "infrastructuralTags": [],
        "measurementUnit": dp_info["dp_unit"],
        "constantExtractionParameters": {"value": dp_info["dp_value"]},
        "valueType": dp_info["dp_value_type"],
        "plantCode": dp_info["plant_code"],
    }
    return template


def create_constant_point(base_url, token, datapoint_info):
    header = {"Authorization": "Bearer " + token}
    template_data = _inject_constant_point_template(datapoint_info)
    return requests.post(
        urljoin(base_url, "/points"), json=template_data, headers=header
    ).json()


# TODO: update docs for enum type datapoint, constant and virtual datapoint
def create_point_if_not_exists(
        base_url, token, point_info, constant=False, virtual=False
):
    r"""
    Create a datapoint using a parameter dictionary and return the
    new datapoint as python dictionary.

    Args:
        point_info:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        datapoint_info (dict):
            parameter dictionary. It must contains the following parameters:

            - ``plant_code`` (str): simonlab code for plant
            - ``device_code`` (str): new datapoint code
            - ``dp_code`` (str): new datapoint code
            - ``dp_name`` (str): new datapoint name
            - ``dp_tag_id`` (str): mongodb id representing the new
              datapoint haystack tag. It must be consistent with the
              device haystack tag.
            - ``dp_is_writable`` (bool): `True` if the new datapoint
              is writable, `False` otherwise

            You can also specify the following optionale parameters:

            - ``dp_value_type``  {"`numeric`", "`boolean`"}: new datapoint
              type. If not specified, new datapoint will be created as "`numeric`"
            - ``dp_description`` (str): new datapoint description
            - ``dp_unit`` (str): new datapoint measurement unit
            - ``dp_value_precision`` (int): new datapoint values precision
            - ``dp_extraction_operator`` {"`Sum`", "`Mean`"}: aggregation
              function for the new datapoint values. If not specified, new
              datapoint will be created using "`Mean`"
            - ``enumDictionary`` (dict): key - value dictionary for the `enum` type datapoint
            - ``dp_driver`` (str): name of the driver for the new
              datapoint (for writable datapoints only)
        constant (bool):
            `True` if the datapoint is constant, `False` otherwise

    Returns:
        dict: new datapoint as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> device_info = {
        >>>     "plant_code" : "0001",
        >>>     "device_code" : "new_device",
        >>>     "dp_code" : "new_datapoint",
        >>>     "dp_name" : "New Datapoint!",
        >>>     "dp_tag_id" : "5d8c9f3c8e7bacf894278d43",
        >>>     "dp_is_writable" : False,
        >>>     "dp_unit" : "m/s",
        >>>     "dp_description" : "velocità del vento"
        >>> }
        >>> datapoint = create_datapoint_if_not_exists(url, token, datapoint_info)
        >>> print(f"{datapoint['description']}, {datapoint['measurementUnit']}")
        velocità del vento, m/s
    """

    def _complete_point_info(base_url, token, point_info):
        driver = None
        if point_info.get("dp_description") is None:
            point_info["dp_description"] = ""
        if point_info.get("dp_unit") is None:
            point_info["dp_unit"] = ""
        if point_info.get("dp_frequency") is None:
            point_info["dp_frequency"] = ""
        if point_info.get("dp_extraction_operator") is None:
            point_info["dp_extraction_operator"] = "Mean"
        if point_info.get("dp_value_type") is None:
            point_info["dp_value_type"] = "numeric"
        if point_info.get("dp_value_precision") is None:
            point_info["dp_value_precision"] = 2
        if point_info.get("dp_enum_dictionary") is None:
            point_info["dp_enum_dictionary"] = []
        if point_info.get("dp_description") is None:
            point_info["dp_description"] = ""
        if point_info.get("device_id") is None:
            point_info["device_id"] = get_device_by_code(
                base_url,
                token,
                point_info["plant_code"],
                point_info["device_code"],
            )["id"]

        if point_info.get("dp_is_writable") is not None:
            if bool(point_info["dp_is_writable"]):
                if point_info.get("dp_driver") is None:
                    point_info["dp_driver"] = "default"

                driver = get_driver_by_name(
                    base_url,
                    token,
                    point_info["plant_code"],
                    point_info["dp_driver"],
                    True,
                )
        if point_info.get("dp_his_mode") is None:
            point_info["dp_his_mode"] = "sampled"
        if point_info.get("dp_his_totalized") is None:
            point_info["dp_his_totalized"] = False
        if point_info.get("dp_his_costant") is None:
            point_info["dp_his_costant"] = False
        if point_info.get("dp_type") is None:
            point_info["dp_type"] = "physical"
        if point_info.get("original_driver_entity_id") is None:
            point_info["original_driver_entity_id"] = None

        return point_info, driver

    def _add_virtual_parameters(point_info):
        interval = point_info["virtual_sampling_interval"]
        method = point_info["virtual_extraction_method"]
        if method == "Custom":
            dp_ids = [
                id.replace("_", "")
                for id in point_info["virtual_expression"].split(" ")
                if len(id) > 0 and id[0] == "_"
            ]
            params = {
                "extractionMethod": method,
                "formalExpression": point_info["virtual_expression"],
                "samplingInterval": interval,
                "involvedDatapointsIds": dp_ids,
                "involvedDatapointId": None,
            }
        else:
            dp_id = point_info["virtual_datapoint_id"]
            params = {
                "extractionMethod": method,
                "formalExpression": "",
                "samplingInterval": interval,
                "involvedDatapointsIds": [dp_id],
                "involvedDatapointId": dp_id,
                "valueValidation": point_info.get("virtual_value_validation") if point_info.get(
                    "virtual_value_validation") else None,
            }
        point_info["virtual_parameters"] = params
        point_info["dp_type"] = "virtual"
        return point_info

    is_writable = False if constant else bool(point_info["dp_is_writable"])
    point = get_point_by_code(
        base_url,
        token,
        point_info["plant_code"],
        point_info["device_code"],
        point_info["dp_code"],
    )
    if point is None:
        new_point_info, driver = _complete_point_info(base_url, token, point_info)
        if constant:
            point = create_constant_point(base_url, token, new_point_info)
        else:
            if virtual:
                new_point_info = _add_virtual_parameters(new_point_info)

            point = create_point(base_url, token, new_point_info, driver)
    return point


def create_alarm(base_url, token, alarm_info):
    header = {"Authorization": "Bearer " + token}
    target_user_ids = [
        get_user_by_email(base_url, token, email)["id"]
        for email in alarm_info["alarm_target_user_ids"]
    ]
    cause_id = get_alarm_cause_by_name(base_url, token, alarm_info["alarm_cause"])

    template_data = {
        "ruleType": alarm_info["rule_type"],
        "name": alarm_info["alarm_name"],
        "conditions": {
            "isIncluded": alarm_info["is_included"],
            "level": alarm_info["alarm_level"],
        },
        "checkInterval": alarm_info["alarm_check_interval"],
        "strikes": alarm_info["alarm_strikes"],
        "deadBand": alarm_info["alarm_dead_band"],
        "sendNotification": alarm_info["alarm_send_notification"],
        "targetUserIds": target_user_ids,
        "triggerActions": {
            "out": {"targetUserIds": target_user_ids, "sendCommand": []},
            "in": {"targetUserIds": target_user_ids, "sendCommand": []},
        },
        "pointId": alarm_info["alarm_point_id"],
        "validityPeriod": alarm_info["validity_period"],
        "alarmCauseId": cause_id,
    }

    if alarm_info["rule_type"] == "cut-off":
        operator = alarm_info["alarm_operator"]
        template_data["conditions"]["operator"] = operator
        template_data["conditions"]["min"] = alarm_info["alarm_min"]

        if ("<" in operator) and (">" in operator):
            template_data["conditions"]["max"] = alarm_info["alarm_max"]

    return requests.post(
        urljoin(base_url, "/alarmRules"), json=template_data, headers=header
    ).json()


def create_alarm_if_not_exists(base_url, token, alarm_info):
    # TODO UPDATE DOCS
    r"""
    Create an alarm rule using a parameter dictionary and return
    the new alarm as python dictionary.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        alarm_info (dict):
            parameter dictionary. It must contains the following parameters:

            - ``plant_code`` (str): simonlab code for plant
            - ``device_code`` (str): new datapoint code
            - ``dp_code`` (str): new datapoint code
            - ``alarm_name`` (str): new alarm name
            - ``alarm_operator`` {"`<`", "`<=`", "`>`", "`>=`", "`<>`", "`><`"}:
              new alarm operator
            - ``alarm_min`` (float): minimum threshold for new alarm rule
            - ``alarm_check_interval`` {`0`, `15`, `60`, `1440`}: number of minutes
              between one notification and another when the alarm remains triggered.
              `0` is a special key that means "you will receive only 1 notification
              for the alarm rule, regardless of how long it remains active"

            You can also specify the following optionale parameters:

            - ``alarm_max`` (float): maximum threshold for new alarm rule. It is
              mandatory when `alarm_operator" is "`<`", "`<=`", "`<>`" or "`><`"
            - ``alarm_level`` {0, 1, 2}: priority of new alarm rule. The possible
              values means (respectively): 'Avviso', 'Grave', 'Bloccante'
            - ``alarm_strikes`` (int): number of consecutive datapoint observations
              that trigger the alarm that must occur before sending the notification
            - ``alarm_dead_band`` (int): new alarm dead band (in percentage)
            - ``alarm_send_notification`` (bool): `True` if you want to send a
              notification email when the alarm is triggered, `False` otherwise
            - ``alarm_target_user_ids`` (list): list of user that receive the alarm
              when the new alarm rule is triggered

    Returns:
        dict: new alarm rule as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> alarm_info = {
        >>>     "plant_code" : "0001",
        >>>     "device_code" : "new_device",
        >>>     "dp_code" : "new_datapoint",
        >>>     "alarm_name" : "Temperatura Maggiore di Dieci",
        >>>     "alarm_min" : ">",
        >>>     "alarm_check_interval" : 1,
        >>>     "dp_unit" : "m/s",
        >>>     "alarm_level" : 0,
        >>>     "alarm_dead_band" : 3,
        >>> }
        >>> alarm = create_alarm_if_not_exists(url, token, alarm_info)
        >>> print(alarm['name'])
        >>> print(f"{alarm['conditions']["min"]}, {alarm['conditions']["operator"]}")
        Temperatura Maggiore di Dieci
        10, >
    """

    def _complete_alarm_info(base_url, token, alarm_info):
        dp = get_point_by_code(
            base_url,
            token,
            alarm_info["plant_code"],
            alarm_info["device_code"],
            alarm_info["dp_code"],
        )
        if alarm_info.get("alarm_dead_band") is None:
            alarm_info["alarm_dead_band"] = 0
        # if alarm_info.get("rule_type") is None:
        #     alarm_info["rule_type"] = "cut-off"
        if alarm_info.get("is_included") is None:
            alarm_info["is_included"] = True
        if alarm_info.get("alarm_level") is None:
            alarm_info["alarm_level"] = 0
        if alarm_info.get("alarm_send_notification") is None:
            alarm_info["alarm_send_notification"] = True
        if alarm_info.get("alarm_strikes") is None:
            alarm_info["alarm_strikes"] = 1
        if alarm_info.get("alarm_target_user_ids") is None:
            alarm_info["alarm_target_user_ids"] = []
        if alarm_info.get("alarm_point_id") is None:
            alarm_info["alarm_point_id"] = dp["_id"]
        if alarm_info.get("validity_period") is None:
            alarm_info["validity_period"] = {
                "months": {
                    "january": True,
                    "february": True,
                    "march": True,
                    "april": True,
                    "may": True,
                    "june": True,
                    "july": True,
                    "august": True,
                    "september": True,
                    "october": True,
                    "november": True,
                    "december": True,
                },
                "days": {
                    "monday": {
                        "isSelected": True,
                        "start": {"hour": "0", "minutes": "00"},
                        "end": {"hour": "0", "minutes": "00"},
                    },
                    "tuesday": {
                        "isSelected": True,
                        "start": {"hour": "0", "minutes": "00"},
                        "end": {"hour": "0", "minutes": "00"},
                    },
                    "wednesday": {
                        "isSelected": True,
                        "start": {"hour": "0", "minutes": "00"},
                        "end": {"hour": "0", "minutes": "00"},
                    },
                    "thursday": {
                        "isSelected": True,
                        "start": {"hour": "0", "minutes": "00"},
                        "end": {"hour": "0", "minutes": "00"},
                    },
                    "friday": {
                        "isSelected": True,
                        "start": {"hour": "0", "minutes": "00"},
                        "end": {"hour": "0", "minutes": "00"},
                    },
                    "saturday": {
                        "isSelected": True,
                        "start": {"hour": "0", "minutes": "00"},
                        "end": {"hour": "0", "minutes": "00"},
                    },
                    "sunday": {
                        "isSelected": True,
                        "start": {"hour": "0", "minutes": "00"},
                        "end": {"hour": "0", "minutes": "00"},
                    },
                },
            }
        return alarm_info, dp

    alarm = get_alarm_by_name(
        base_url,
        token,
        alarm_info["plant_code"],
        alarm_info["device_code"],
        alarm_info["dp_code"],
        alarm_info["alarm_name"],
    )

    if len(alarm) == 0:
        new_alarm_info, datapoint = _complete_alarm_info(base_url, token, alarm_info)
        alarm = create_alarm(base_url, token, new_alarm_info)

        datapoint = _prune(datapoint)

        # TODO: a cosa serve?
        # datapoint["alarmRuleIds"] = datapoint["alarmRuleIds"] + [alarm["id"]]
        header = {"Authorization": "Bearer " + token}
        requests.put(
            urljoin(base_url, f"/points/{datapoint['id']}"),
            json=datapoint,
            headers=header,
        ).json()

    return alarm


def create_chart(base_url, token, chart_info):
    header = {"Authorization": "Bearer " + token}

    if chart_info.get("parent_type") is None:
        chart_info["parent_type"] = "Device"
    if chart_info.get("overrideNumberOfPoints") is None:
        chart_info["overrideNumberOfPoints"] = 100
    template_data = {
        "name": chart_info["chart_name"],
        "pointIds": chart_info["chart_p_id"],
        "chartType": chart_info["chart_type"],
        "softMin": chart_info["chart_soft_min"],
        "softMax": chart_info["chart_soft_max"],
        "isCloned": chart_info["chart_is_cloned"],
        "overrideNumberOfPoints": chart_info["overrideNumberOfPoints"],
        # "pointIds": chart_info["chart_ps_id"],
        "series": chart_info["chart_series"],
        "parentType": chart_info["parent_type"],
        "parentId": chart_info["parent_id"],
    }
    if chart_info.get("chart_timeframe"):
        template_data["timeFrame"] = chart_info["chart_timeframe"]
    else:
        template_data["timeFrame"] = {
            "window": chart_info["chart_window"],
            "comparewindow": chart_info["chart_compare_window"],
            "from": chart_info["chart_from"],
            "to": chart_info["chart_to"],
            "comparefrom": chart_info["chart_compare_from"],
            "compareto": chart_info["chart_compare_to"],
            "aggregation": chart_info["chart_aggregation"],
        }

    return requests.post(
        urljoin(base_url, "/charts"), json=template_data, headers=header
    ).json()


def create_dashboard(base_url, token, dashboard_info):
    header = {"Authorization": "Bearer " + token}
    template_data = {
        "name": dashboard_info["dashboard_name"],
        "minimumVisibilityRole": dashboard_info["dashboard_visibility_role"],
        "presetTimeFrame": dashboard_info["dashboard_time_frame"],
        "assignedUserIds": [],
        "orderIndex": dashboard_info["dashboard_order_index"],
        "referenceRole": dashboard_info["dashboard_reference_role"],
        "parentId": dashboard_info["parent_id"],
        "parentType": dashboard_info["parent_type"],
        "widgets": dashboard_info["dashboard_widgets"],
    }
    return requests.post(
        urljoin(base_url, "/dashboards"), json=template_data, headers=header
    ).json()


def create_empty_dashboard(base_url, token, dashboard_info):
    dashboard_info["dashboard_widgets"] = []
    return create_dashboard(base_url, token, dashboard_info)


# TODO: da controllare
def create_dashboard_if_not_exists(base_url, token, dashboard_info, empty=True):
    # TODO: SISTEMARE I PARAMETRI NELLA DOCS
    r"""
    Create a dashboard using a parameter dictionary and return the
    new dashboard as python dictionary.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        dashboard_info (dict):
            parameter dictionary. It must contains the following parameters:

            - ``parent_code`` (str): simonlab code for the parent
            - ``dashboard_name`` (str): new dashboard name
            - ``parent_type`` (str): new dashboard parent type. Must be
                "`Plant`" or "`Project`"

            You can also specify the following optionale parameters:

            - ``dashboard_visibility_role`` (str):
            - ``dashboard_order_index`` (str):
            - ``dashboard_reference_role`` (str):
        empty (bool):
            `True` if the dashboard is empty, `False` otherwise

    Returns:
        dict: new dashboard as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> dashboard_info = {
        >>>     "parent_code" : "0001",
        >>>     "parent_type" : "Plant",
        >>>     "dashboard_name" : "New Dashboard!"
        >>> }
        >>> dashboard = create_dashboard_if_not_exists(url, token, dashboard_info)
        >>> print(dashboard["name"])
        New Dashboard!
    """

    def _complete_dashboard_info(base_url, token, dashboard_info):
        if dashboard_info.get("dashboard_visibility_role") is None:
            dashboard_info["dashboard_visibility_role"] = "admin"
        if dashboard_info.get("dashboard_order_index") is None:
            dashboard_info["dashboard_order_index"] = 1
        if dashboard_info.get("dashboard_reference_role") is None:
            dashboard_info["dashboard_reference_role"] = "admin"

        if dashboard_info.get("parent_id") is None:
            if dashboard_info.get("parent_type") == "Project":
                dashboard_info["parent_id"] = get_project_by_code(
                    base_url, token, dashboard_info["parent_code"]
                )["id"]
            else:
                dashboard_info["parent_id"] = get_plant_by_code(
                    base_url, token, dashboard_info["parent_code"]
                )["id"]

        return dashboard_info

    dashboard = get_dashboard_by_name(
        base_url,
        token,
        str(dashboard_info["parent_code"]),
        dashboard_info["dashboard_name"],
        dashboard_info.get("parent_type"),
    )
    if dashboard is None:
        new_dashboard_info = _complete_dashboard_info(base_url, token, dashboard_info)
        if empty:
            dashboard = create_empty_dashboard(base_url, token, new_dashboard_info)
        else:
            dashboard = create_dashboard(base_url, token, new_dashboard_info)

    return dashboard


# UPDATE


def update_device_if_exists(base_url, token, device_info):
    r"""
    Update an existing device using a parameter dictionary and return the
    updated device as python dictionary.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        device_info (dict):
            parameter dictionary. It must contains the following parameters:

            - ``plant_code`` (str): simonlab code for plant
            - ``device_code`` (str): simonlab code for device

            This parameter are used to identify the exact device.
            You can also specify the parameters you want to update:

            - ``device_name`` (str): new device name
            - ``device_description`` (str): new device description
            - ``device_icon_id`` (str): mongodb id for the new device icon

    Returns:
        dict: updated device as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> device_info = {
        >>>     "plant_code" : "0001",
        >>>     "device_code" : "StazioneMeteo",
        >>>     "device_description" : "Stazione meteo di 'The Bridge'"
        >>> }
        >>> device = update_device_if_exists(url, token, device_info)
        >>> print(device["description"])
        Stazione meteo di 'The Bridge'
    """

    def _update_device(device, device_info):
        if device_info.get("device_name") is not None:
            device["name"] = device_info["device_name"]
        if device_info.get("device_description") is not None:
            device["description"] = device_info["device_description"]
        if device_info.get("device_icon_id") is not None:
            device["iconId"] = device_info["device_icon_id"]
        if device_info.get("device_tag_id") is not None:
            device["customTags"]["tagId"] = device_info["device_tag_id"]
        if device_info.get("parentType") is not None:
            device["parentType"] = device_info["parentType"]
        if device_info.get("parent_id") is not None:
            device["parentId"] = device_info["parent_id"]
        return device

    device = None
    device = get_device_by_code(
        base_url, token, device_info["plant_code"], device_info["device_code"]
    )
    if device is not None:
        device = _prune(device)
        device = _update_device(device, device_info)

        header = {"Authorization": "Bearer " + token}
        url = urljoin(base_url, f"/devices/{device['id']}")
        device = requests.put(url, json=device, headers=header).json()

    return device


def update_point_if_exists(base_url, token, point_info):
    r"""
    Update an existing datapoint using a parameter dictionary and return the
    updated datapoint as python dictionary.

    Args:
        base_url (str):
            base url for simonlab (prod or dev)
        token (str):
            bearer token for simonlab (prod or dev)
        datapoint_info (dict):
            parameter dictionary. It must contains the following parameters:

            - ``plant_code`` (str): simonlab code for plant
            - ``device_code`` (str): simonlab code for device
            - ``dp_code`` (str): simonlab code for datapoint
            - ``dp_is_writable`` (bool): `True` if the datapoint is writable,
              `False` otherwise

            This parameter are used to identify the exact datapoint.
            You can also specify the parameters you want to update:

            - ``dp_name`` (str): new datapoint name
            - ``dp_description`` (str): new datapoint description
            - ``dp_unit`` (str): new datapoint measurement unit
            - ``dp_extraction_operator`` {"`Sum`", "`Mean`"}: new aggregation
              function for the datapoint values.  If not specified, new
              datapoint will be created using "`Mean`"
            - ``dp_value_precision`` (int): new precision for datapoint values
            - ``dp_tag_id`` (str): new heystack tag mongodb id
            - ``dp_driver`` (str): name of the new driver (for
              writable datapoints only)
            - ``valueType``  {"`boolean`", "`numeric`", "`enum`"}: new datapoint type
            - ``enumDictionary`` (dict): new dictionary for the `enum` type datapoint
            - ``dp_frequency`` (dict): new datapoint sampling frequency

    Returns:
        dict: updated datapoint as python dictionary

    Examples:
        >>> url = os.getenv("SIMON_URL")
        >>> token = os.getenv("SIMON_TOKEN")
        >>> datapoint_info = {
        >>>     "plant_code" : "0001",
        >>>     "device_code" : "StazioneMeteo",
        >>>     "dp_code" : "temp",
        >>>     "dp_is_writable" : False,
        >>>     "dp_description" : "Temperatura Esterna",
        >>>     "dp_value_precision" : 2,
        >>> }
        >>> datapoint = update_datapoint_if_exists(url, token, device_info)
        >>> print(f"{datapoint['description']}, {datapoint['valuePrecision']}")
        Temperatura Esterna, 2
    """

    def _update_datapoint(base_url, token, datapoint, point_info):
        if point_info.get("dp_name") is not None:
            datapoint["name"] = point_info["dp_name"]
        if point_info.get("dp_frequency") is not None:
            datapoint["estimatedDataCadence"] = point_info["dp_frequency"]
        if point_info.get("dp_description") is not None:
            datapoint["description"] = point_info["dp_description"]
        if point_info.get("dp_unit") is not None:
            datapoint["measurementUnit"] = point_info["dp_unit"]
        if point_info.get("dp_extraction_operator") is not None:
            datapoint["extractionOperator"] = point_info["dp_extraction_operator"]
        if point_info.get("dp_value_precision") is not None:
            datapoint["valuePrecision"] = point_info["dp_value_precision"]
        if point_info.get("dp_tag") is not None:
            datapoint["customTags"]["tagId"] = point_info["dp_tag"]

        if point_info.get("dp_value_type") is not None:
            datapoint["valueType"] = point_info["dp_value_type"]
        if point_info.get("dp_enum_dictionary") is not None:
            datapoint["enumDictionary"] = point_info["dp_enum_dictionary"]

        if point_info.get("virtual_sampling_interval") is not None:
            datapoint["virtualParameters"]["samplingInterval"] = point_info[
                "virtual_sampling_interval"
            ]

        if point_info.get("virtual_expression") is not None:
            datapoint["virtualParameters"]["formalExpression"] = point_info[
                "virtual_expression"
            ]
            dp_ids = [
                id[1:]
                for id in point_info.get("virtual_expression").split(" ")
                if id[0] == "_"
            ]
            datapoint["virtualParameters"]["involvedDatapointsIds"] = dp_ids

        if point_info.get("virtual_datapoint_id") is not None:
            datapoint["virtualParameters"]["involvedDatapointId"] = point_info[
                "virtual_datapoint_id"
            ]
            datapoint["virtualParameters"]["involvedDatapointsIds"] = [
                point_info["virtual_datapoint_id"]
            ]

        if point_info.get("dp_value") is not None:
            datapoint["constantExtractionParameters"]["value"] = point_info["dp_value"]

        print("driver -> ", point_info.get("dp_driver"))
        if point_info.get("dp_driver") is not None:
            driver = get_driver_by_name(
                base_url,
                token,
                point_info["plant_code"],
                point_info["dp_driver"],
            )
            datapoint["originalDriverId"] = driver["id"]

        if point_info.get("dp_his_mode") is not None:
            datapoint["hisMode"] = point_info["dp_his_mode"]
        if point_info.get("dp_his_totalized") is not None:
            datapoint["hisTotalized"] = point_info["dp_his_totalized"]

        if point_info.get("original_driver_entity_id") is not None:
            datapoint["originalDriverEntityId"] = point_info["original_driver_entity_id"]

        return datapoint

    point = None
    point = get_point_by_code(
        base_url,
        token,
        point_info["plant_code"],
        point_info["device_code"],
        point_info["dp_code"],
    )
    if point is not None:
        point = _prune(point)
        point = _update_datapoint(base_url, token, point, point_info)

        header = {"Authorization": "Bearer " + token}
        datapoint = requests.put(
            urljoin(base_url, f"/points/{point['id']}"), json=point, headers=header
        ).json()

    return point


def update_alarm_if_exists(base_url, token, alarm_info):
    # TODO: documentazione
    def _update_alarm(base_url, token, alarm, alarm_info):
        # if alarm_info.get("alarm_name") is not None:
        # alarm["name"] = alarm_info["alarm_name"]
        if alarm_info.get("alarm_operator") is not None:
            alarm["conditions"]["operator"] = alarm_info["alarm_operator"]
        if alarm_info.get("alarm_min") is not None:
            alarm["conditions"]["min"] = alarm_info["alarm_min"]
        if alarm_info.get("alarm_max") is not None:
            alarm["conditions"]["max"] = alarm_info["alarm_max"]
        if alarm_info.get("is_included") is not None:
            alarm["conditions"]["isIncluded"] = alarm_info["is_included"]
        if alarm_info.get("alarm_dead_band") is not None:
            alarm["deadBand"] = alarm_info["alarm_dead_band"]
        if alarm_info.get("alarm_level") is not None:
            alarm["conditions"]["level"] = alarm_info["alarm_level"]
        if alarm_info.get("alarm_check_interval") is not None:
            alarm["checkInterval"] = alarm_info["alarm_check_interval"]
        if alarm_info.get("alarm_send_notification") is not None:
            alarm["sendNotification"] = alarm_info["alarm_send_notification"]
        if alarm_info.get("alarm_strikes") is not None:
            alarm["strikes"] = alarm_info["alarm_strikes"]
        if alarm_info.get("validity_period") is not None:
            alarm["validityPeriod"] = alarm_info["validity_period"]
        if alarm_info.get("alarm_target_user_ids") is not None:
            user_ids = [
                get_user_by_email(base_url, token, email)
                for email in alarm_info["alarm_target_user_ids"]
            ]
            alarm["triggerActions"]["in"]["targetUserIds"] = user_ids
            alarm["triggerActions"]["out"]["targetUserIds"] = user_ids
            alarm["targetUserIds"] = [email["id"] for email in user_ids]

        if alarm_info.get('strikes_time') is not None:
            alarm["strikesTime"] = alarm_info['strikes_time']
        return alarm

    alarm = None
    alarm = get_alarm_by_name(
        base_url,
        token,
        alarm_info["plant_code"],
        alarm_info["device_code"],
        alarm_info["dp_code"],
        alarm_info["alarm_name"],
        single=True,
    )
    print(len(alarm))
    if len(alarm) == 1:
        alarm = alarm[0]
        alarm = _update_alarm(base_url, token, alarm, alarm_info)
        alarm = _prune(alarm)

        header = {"Authorization": "Bearer " + token}
        url = urljoin(base_url, f"/alarmRules/{alarm['id']}")
        alarm = requests.put(url, json=alarm, headers=header).json()

    return alarm


# TODO: da controllare
# def update_dashboard_if_exists(base_url, token, dashboard_info):
#     r"""
#     Update an existing dashboard using a parameter dictionary and return the
#     updated dashboard as python dictionary.
#
#     Args:
#         base_url (str):
#             base url for simonlab (prod or dev)
#         token (str):
#             bearer token for simonlab (prod or dev)
#         dashboard_info (dict):
#             parameter dictionary. It must contains the following parameters:
#
#             - ``parent_code`` (str): simonlab code for the parent
#             - ``parent_type`` (str): new dashboard parent type. Must be
#                 "`Plant`" or "`Project`"
#             - ``dashboard_name`` (str): simonlab name for dashboard
#
#             This parameter are used to identify the exact dashboard.
#             You can also specify the parameters you want to update:
#
#             - ``new_dashboard_widgets`` (str): new dashboard widget
#
#     Returns:
#         dict: updated dashboard as python dictionary
#
#     Examples:
#         >>> url = os.getenv("SIMON_URL")
#         >>> token = os.getenv("SIMON_TOKEN")
#         >>> dashboard_info = {
#         >>>     "parent_code" : "0001",
#         >>>     "parent_type" : "Plant",
#         >>>     "dashboard_name" : "Empty Dashboard",
#         >>>     "new_dashboard_widgets" : ???????????????
#         >>> }
#         >>> dashboard = update_dashboard_if_exists(url, token, dashboard_info)
#         >>> print(len(dashboard["widgets"]))
#         1
#     """
#
#     def _update_dashboard(dashboard, dashboard_info):
#         if dashboard_info.get("new_dashboard_widgets") is not None:
#             for widget in dashboard_info["new_dashboard_widgets"]:
#                 # TODO: sto supponendo di avere i widget già pronti
#                 # in alternativa dovrei "crearli"...
#
#                 # if widget["widgetType"] == "chart":
#                 #     chart_widget =
#                 #     dashboard["widgets"].append(chart_widget)
#                 # else:
#                 dashboard["widgets"].append(widget)
#         return dashboard
#
#     dashboard = None
#     dashboard = get_dashboard_by_name(
#         base_url,
#         token,
#         str(dashboard_info["parent_code"]),
#         dashboard_info["dashboard_name"],
#         dashboard_info.get("parent_type"),
#     )
#     if dashboard is not None:
#         dashboard = _prune(dashboard)
#         dashboard = _update_dashboard(dashboard, dashboard_info)
#
#         header = {"Authorization": "Bearer " + token}
#         url = urljoin(base_url, f"/dashboards/{dashboard['id']}")
#         dashboard = requests.put(url, json=dashboard, headers=header).json()
#
#     return dashboard


# DELETE


def delete_alarm_if_exists(base_url, token, alarm_info):
    # TODO: documentazione
    alarm = None
    alarm = get_alarm_by_name(
        base_url,
        token,
        alarm_info["plant_code"],
        alarm_info["device_code"],
        alarm_info["dp_code"],
        alarm_info["alarm_name"],
    )
    if alarm is not None:
        header = {"Authorization": "Bearer " + token}
        url = urljoin(base_url, f"/alarmRules/{alarm[0]['id']}")
        req = requests.delete(url, headers=header)

        if req.status_code == 204:
            alarm = None

    return alarm


def delete_device_if_exists(base_url, token, device_info):
    # TODO: documentazione
    device = None
    device = get_device_by_code(
        base_url, token, device_info["plant_code"], device_info["device_code"]
    )
    if device is not None:
        header = {"Authorization": "Bearer " + token}
        url = urljoin(base_url, f"/devices/{device['id']}")
        req = requests.delete(url, headers=header)

        if req.status_code == 204:
            device = None

    return device


def delete_point_if_exists(base_url, token, point_info):
    # TODO: documentazione
    point = None
    point = get_point_by_code(
        base_url,
        token,
        point_info["plant_code"],
        point_info["device_code"],
        point_info["dp_code"],
    )
    if point is not None:
        header = {"Authorization": "Bearer " + token}
        url = urljoin(base_url, f"/points/{point['id']}")
        req = requests.delete(url, headers=header)

        if req.status_code == 204:
            point = None

    return point


# TODO: da controllare
# def delete_dashboard_if_exists(base_url, token, dashboard_info):
#     # TODO: documentazione
#     dashboard = None
#     dashboard = get_dashboard_by_name(
#         base_url,
#         token,
#         str(dashboard_info["parent_code"]),
#         dashboard_info["dashboard_name"],
#         dashboard_info.get("parent_type"),
#     )
#     if dashboard is not None:
#         header = {"Authorization": "Bearer " + token}
#         url = urljoin(base_url, f"/dashboards/{dashboard['id']}")
#         req = requests.delete(url, headers=header)
#
#         if req.status_code == 204:
#             dashboard = None
#
#     return dashboard
def delete_chart_if_exists(base_url, token, chart_id):
    # TODO: documentazione e sarebbe da fare la get chart by name, deviceCode e/o type
    chart = None
    chart = get_chart_by_id(base_url, token, chart_id)
    if chart is not None:
        header = {"Authorization": "Bearer " + token}
        url = urljoin(base_url, f"/charts/{chart['id']}")
        req = requests.delete(url, headers=header)

        if req.status_code == 204:
            chart = None

    return chart
