from setuptools import setup

setup(
    name="ecologiclib",
    version="1.1.1",
    description="Evogy EcoLogicAI Library",
    url="https://eldritchhh@bitbucket.org/evogy/ecologiclib.git",
    author="Francesco Prete",
    author_email="francesco.prete@evogy.it",
    license="unlicense",
    packages=["ecologiclib", "ecologiclib.db"],
    install_requires=[
        "pandas",
        "requests",
        "pymodm",
        "pika",
    ],
    extra_requires=[
        "sphinx",
        "sphinx-autobuild",
        "myst-parser",
    ],
    zip_safe=False,
)
