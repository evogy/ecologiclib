import unittest
from unittest.mock import Mock, patch

from ecologiclib.simonlab_object_crud import (
    get_driver_by_id,
    get_project_by_id,
    get_project_by_code,
    get_plant_by_id,
    get_plant_by_code,
    get_device_by_id,
    get_device_by_code,
    create_device_if_not_exists,
    update_device_if_exists,
    delete_device_if_exists,
    create_point_if_not_exists,
    get_point_by_code,
    update_point_if_exists,
    delete_point_if_exists, get_space_by_name, get_space_by_id, get_device_by_plant_code, create_chart,
)

SIMON_URL = "https://demo-api.simondigital.it"
SIMON_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZWQwMWIxYzYzYjViMDdkODRjZTZhOCIsImN1c3RvbS1leHAiOjE3MDczMDIzODUsImlhdCI6MTcwNzI5NTE4NX0.LO48fvQ8p0z7tovH-E2nodPYm2wBlROVWESegGjyWAc"


# region TEST SIMON 3
class TestProject(unittest.TestCase):
    def test_get_project_by_id(self):
        # TODO: capire perché non funziona
        # {"errorCode":0,"statusCode":400,"defaultMessage":"expecting an array or an iterable object but got [object Null]"}
        res = get_project_by_id(
            SIMON_URL, SIMON_TOKEN, project_id="5b31f615202a5000145ab603"
        )
        self.assertEqual(res["id"], "5b31f615202a5000145ab603")

    def test_get_project_by_code(self):
        res = get_project_by_code(SIMON_URL, SIMON_TOKEN, project_code="Simon")
        self.assertEqual(res["code"], "Simon")


class TestPlant(unittest.TestCase):
    def test_01_get_plant_by_id(self):
        res = get_plant_by_id(
            SIMON_URL, SIMON_TOKEN, plant_id="5b31f65a202a5000145ab604"
        )
        self.assertEqual(res["id"], "5b31f65a202a5000145ab604")

    def test_02_get_plant_by_code(self):
        res = get_plant_by_code(SIMON_URL, SIMON_TOKEN, plant_code="0001")
        self.assertEqual(res["code"], "0001")


class TestDeviceCRUD(unittest.TestCase):
    def test_get_device_by_id(self):
        res = get_device_by_id(
            SIMON_URL, SIMON_TOKEN, device_id="659e816c313cdd0013ce2c6d"
        )
        self.assertEqual(res["id"], "659e816c313cdd0013ce2c6d")

    def test_01_create_device_if_not_exists(self):
        plant = get_plant_by_code(SIMON_URL, SIMON_TOKEN, plant_code="0001")
        res = create_device_if_not_exists(
            SIMON_URL,
            SIMON_TOKEN,
            device_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "device_name": "testCreateDeviceIfNotExists",
                "device_description": "testCreateDeviceIfNotExists - ecologiclib",
                "device_tag": "SONDA_GENERICA",
            },
        )

        self.assertEqual(res["code"], "testCreateDeviceIfNotExists")

    def test_02_get_device_by_code(self):
        res = get_device_by_code(
            SIMON_URL,
            SIMON_TOKEN,
            plant_code="0001",
            device_code="testCreateDeviceIfNotExists",
        )
        self.assertEqual(res["code"], "testCreateDeviceIfNotExists")

    def test_03_update_device_if_exists(self):
        plant = get_plant_by_code(SIMON_URL, SIMON_TOKEN, plant_code="0001")
        res = update_device_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            device_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "device_name": "testCreateDeviceIfNotExists",
                "device_description": "testCreateDeviceIfNotExists MODIFICATA - ecologiclib",
            },
        )
        self.assertEqual(
            res["description"], "testCreateDeviceIfNotExists MODIFICATA - ecologiclib"
        )

    def test_04_delete_device_if_exists(self):
        plant = get_plant_by_code(SIMON_URL, SIMON_TOKEN, plant_code="0001")
        res = delete_device_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            device_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
            },
        )

        self.assertEqual(res, None)

    def test_05_get_device_by_plant(self):
        res = get_device_by_plant_code(
            SIMON_URL,
            SIMON_TOKEN,
            plant_code="12345",
        )
        self.assertEqual(res[0]["code"], "12345")


class TestPointCRUD(unittest.TestCase):
    def test_00_create_device(self):
        plant = get_plant_by_code(SIMON_URL, SIMON_TOKEN, plant_code="0001")
        res = create_device_if_not_exists(
            SIMON_URL,
            SIMON_TOKEN,
            device_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "device_name": "testCreateDeviceIfNotExists",
                "device_description": "testCreateDeviceIfNotExists - ecologiclib",
                "device_tag": "SONDA_GENERICA",
            },
        )

        self.assertEqual(res["code"], "testCreateDeviceIfNotExists")

    def test_01_create_point_readable(self):
        res = create_point_if_not_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointReadable",
                "dp_name": "pointReadable",
                "dp_is_writable": False,
                "dp_tag": "ZT",
                "dp_frequency": "60",
            },
        )
        self.assertEqual(res["code"], "pointReadable")

    def test_02_create_point_writable(self):
        res = create_point_if_not_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointWritable",
                "dp_name": "pointWritable",
                "dp_is_writable": True,
                "dp_tag": "HUMIDITY2",
                "dp_frequency": "60",
            },
        )
        self.assertEqual(res["code"], "pointWritable")

    def test_03_create_point_costant(self):
        res = create_point_if_not_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointCostant",
                "dp_name": "pointCostant",
                "dp_is_writable": False,
                "dp_tag": "ZT",
                "dp_value": 1,
            },
            constant=True,
        )
        self.assertEqual(res["code"], "pointCostant")

    def test_04_create_virtual_point_custom(self):
        # TODO: per andare a fare le operazioni bisogna passare l'id del datapoint readable associato al point
        point1_id = get_point_by_code(
            SIMON_URL,
            SIMON_TOKEN,
            plant_code="0001",
            device_code="testCreateDeviceIfNotExists",
            point_code="pointReadable",
        )["readableId"]
        point2_id = get_point_by_code(
            SIMON_URL,
            SIMON_TOKEN,
            plant_code="0001",
            device_code="testCreateDeviceIfNotExists",
            point_code="pointCostant",
        )["readableId"]

        res = create_point_if_not_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointVirtualCustom",
                "dp_name": "pointVirtualCustom",
                "dp_is_writable": False,
                "dp_tag": "LTLVL",
                "virtual_sampling_interval": 15,
                "virtual_extraction_method": "Custom",
                "virtual_expression": f"_{point1_id} / _{point2_id}",
                "dp_frequency": "60",
            },
            virtual=True,
        )
        self.assertEqual(res["code"], "pointVirtualCustom")

    def test_05_create_virtual_point_method(self):
        point1_id = get_point_by_code(
            SIMON_URL,
            SIMON_TOKEN,
            plant_code="12345",
            device_code="hwm_02",
            point_code="Flow",
        )["readableId"]

        res = create_point_if_not_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "12345",
                "device_code": "hwm_02",
                "dp_code": "testValueVAlidation",
                "dp_name": "testValueVAlidation",
                "dp_is_writable": False,
                "dp_tag": "THRMEN",
                "virtual_sampling_interval": 15,
                "virtual_extraction_method": "Difference",
                "virtual_datapoint_id": point1_id,
                "virtual_value_validation": "Positive",
                "estimatedDataCadence": "60",
            },
            virtual=True,
        )
        import pdb;
        pdb.set_trace()
        self.assertEqual(res["code"], "pointVirtualDerived")

    def test_06_update_point(self):
        res = update_point_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointReadable",
                "dp_name": "pointReadable",
                "dp_description": "pointReadable UPDATED- ecologiclib",
                "dp_tag": "LTLVL",
                "dp_is_writable": False,
            },
        )
        self.assertEqual(
            {"desc": res["description"], "tag": res["customTags"]["tagId"]},
            {"desc": "pointReadable UPDATED- ecologiclib", "tag": "LTLVL"},
        )

    def test_07_delete_point(self):
        point5 = delete_point_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointVirtualDerived",
            },
        )
        point4 = delete_point_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointVirtualCustom",
            },
        )
        point3 = delete_point_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointCostant",
            },
        )
        point2 = delete_point_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointWritable",
            },
        )
        point1 = delete_point_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            point_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
                "dp_code": "pointReadable",
            },
        )

        self.assertEqual(
            {
                "pointVirtualDerived": point5,
                "pointVirtualCustom": point4,
                "pointCostant": point3,
                "pointWritable": point2,
                "pointReadable": point1,
            },
            {
                "pointVirtualDerived": None,
                "pointVirtualCustom": None,
                "pointCostant": None,
                "pointWritable": None,
                "pointReadable": None,
            },
        )

    def test_08_delete_device(self):
        device = delete_device_if_exists(
            SIMON_URL,
            SIMON_TOKEN,
            device_info={
                "plant_code": "0001",
                "device_code": "testCreateDeviceIfNotExists",
            },
        )
        self.assertEqual(device, None)


class TestCreateSpaceCRUD(unittest.TestCase):
    def test_00_create_space(self):
        res = create_space_if_not_exists(
            SIMON_URL,
            SIMON_TOKEN,
            space_info={
                "type": "Building",
                "space_code": "testSpace",
                "space_name": "testSpace",
                "parent_id": "5b31f65a202a5000145ab604",
                "parent_type": "Plant",
                "plant_code": "0001",
            },
        )
        self.assertEqual(res["code"], "testSpace")

    def test_01_get_space_by_name(self):
        res = get_space_by_name(
            SIMON_URL,
            SIMON_TOKEN,
            space_name="Stanza 0",
            plant_code="9999",
            # parent_type="Plant"
        )
        print(res)
        self.assertEqual(res[0]["code"], "edificio_0")

    def test_02_get_space_by_id(self):
        res = get_space_by_id(
            SIMON_URL,
            SIMON_TOKEN,
            space_id="65eac2e6cceca20024364dd9"
        )
        self.assertEqual(res["id"], "65eac2e6cceca20024364dd9")


class TestChart(unittest.TestCase):
    def test_00_create_chart(self):
        req = create_chart(
            SIMON_URL,
            SIMON_TOKEN,
            chart_info={
                "parent_id": "65eaf2885ea813002795b117",
                "chart_name": "Analisi Consumi2",
                "chart_type": "multiAxes",
                "chart_soft_min": None,
                "chart_soft_max": None,
                "chart_is_cloned": False,
                "chart_p_id": [
                    "65eaf2885ea813002795b121",
                    "65eaf289d42f4c0025dc4727",
                ],
                "chart_series": [
                    {
                        "name": "Thermal Energy",
                        "aggregation": "Sum",
                        "pointIds": [
                            "65eaf2885ea813002795b121"
                        ],
                        "color": "#41729F"
                    },
                    {
                        "name": "Thermal Energy",
                        "aggregation": "Sum",
                        "pointIds": [
                            "65eaf289d42f4c0025dc4727"
                        ],
                        "color": "#E43D40"
                    }
                ],
                "chart_timeframe": {
                    "window": "last_5_days",
                    "comparewindow": "no_compare",
                    "comparefrom": None,
                    "compareto": None,
                    "aggregation": "15min"
                },
            },
        )
        print(req)


# endregion


if __name__ == "__main__":
    unittest.main()
