from ecologiclib.simonlab_object_crud import (
    get_plant_by_code,
    delete_device_if_exists,
    update_device_if_exists,
    get_point_by_code,
    create_point_if_not_exists,
    create_chart,
    get_chart_by_id,
    delete_chart_if_exists, get_project_by_id,
)
res = create_space_if_not_exists(
    SIMON_URL,
    SIMON_TOKEN,
    space_info={
        "type": "Building",
        "space_code": "testSpace",
        "space_name": "testSpace",
        "parent_id": "659e55cfc5465200136ecba7",
        "parent_type": "Plant",
        "plant_code": "1597535",
    },
)

res = create_space_if_not_exists(
    SIMON_URL,
    SIMON_TOKEN,
    space_info={
        "type": "Floor",
        "space_code": "testSpaceFloor",
        "space_name": "testSpaceFloor",
        "parent_id": "65bccb4aacfb6a0014ce2d33",
        "parent_type": "Space",
        "plant_code": "1597535",
    },
)
