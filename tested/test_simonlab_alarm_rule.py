from ecologiclib import create_alarm_if_not_exists

alarm_info = {
    "plant_code": "0001",  # MANDATORY
    "device_code": "testDevicePipeLib",  # MANDATORY
    "dp_code": "testPointPipeNodeLib",  # MANDATORY
    "alarm_name": "test_manual-new-alarms con mail",  # MANDATORY
    "alarm_cause": "Batteria scarica",
    "rule_type": "cut-off",
    "alarm_check_interval": 10,
    "alarm_operator": ">",
    "alarm_min": 0,
    "alarm_target_user_ids": ["stefano.villa@evogy.it"],
    "alarm_max": 400,
}

res = create_alarm_if_not_exists(
    SIMON_URL,
    SIMON_TOKEN,
    alarm_info,
)
