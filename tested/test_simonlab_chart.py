from ecologiclib.simonlab_object_crud import (
    get_plant_by_code,
    delete_device_if_exists,
    update_device_if_exists,
    get_point_by_code,
    create_point_if_not_exists,
    create_chart,
    delete_chart_if_exists,
)

res = create_chart(
    SIMON_URL,
    SIMON_TOKEN,
    chart_info={
        "chart_name": "creato da API3 2asd",
        "chart_type": "timeSeriesZoomable",
        "chart_soft_min": 0,
        "chart_soft_max": 100,
        "chart_is_cloned": False,
        "chart_series": [
            {
                "name": "Test",
                "color": "#FF0000",
                "aggregation": "Mean",
                "pointIds": ["65d71cc6ef7cf7001494a899"],
            }
        ],
        "chart_timeframe": {
            "window": "last_5_days",
            "comparewindow": "no_compare",
            "comparefrom": None,
            "compareto": None,
            "aggregation": "15min",
        },
        "parent_type": "Device",  # ['Project', 'Plant', 'Space', 'Device']
        "parent_id": "65d71c93ef7cf7001494a055",  # id del parent riferito al parent Type
    },
)
print(res)

# delete = delete_chart_if_exists(SIMON_URL, SIMON_TOKEN, "65a6a5ae9dafbf00130b95a7")
#
# print(res)
